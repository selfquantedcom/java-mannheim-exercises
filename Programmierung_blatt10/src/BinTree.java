//Friedl, 1571906

class BinTree {
	private Comparable item;
	private BinTree left, right;

	public void insert(Comparable elem) {
		if (item == null) {
			item = elem;
			left = new BinTree();
			right = new BinTree();
		} else {
			BinTree next;
			if (item.compareTo(elem) > 0)
				next = left;
			else
				next = right;
			next.insert(elem);
		}
	}

	public void insertLoop(Comparable elem) {

		BinTree tempTree = this;

		while (true) {
			if (tempTree.item == null) {
				tempTree.item = elem;
				tempTree.left = new BinTree();
				tempTree.right = new BinTree();

				break;
			}
			if (tempTree.item.compareTo(elem) > 0) {
				tempTree = tempTree.left;

			} else {
				tempTree = tempTree.right;

			}

		}

	}

	public Comparable search(Comparable elem) {
		if (item == null)
			return null;
		int res = item.compareTo(elem);
		if (res == 0)
			return item;
		return ((res > 0) ? left : right).search(elem);
	}

	public String toString() {
		if (item == null)
			return "";
		return left.toString() + item.toString() + " " + right.toString();
	}

	public static void main(String[] args) {
		BinTree tree = new BinTree();

		tree.insertLoop(8);
		tree.insertLoop(3);
		tree.insertLoop(9);
		tree.insertLoop(4);
		tree.insertLoop(7);
		tree.insertLoop(18);
		tree.insertLoop(44);
		tree.insertLoop(2);
		tree.insertLoop(1);
		System.out.println(tree);

	}
}
