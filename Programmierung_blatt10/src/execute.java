
public class execute {

	public static void main(String[] args) {
		SnakeList s = new SnakeList();
		SnakeList s2 = new SnakeList();
		SnakeList s3 = new SnakeList();
		
		//To connect the elements
		s.nextElement = s2;
		s2.nextElement = s3;
		
		//To create a SnakeList of length x
		int x = 5;
		while(x>0){
			s.nextElement=new SnakeList();
			s.nextElement.head=false;
			s=s.nextElement;
			x--;
			
		}
		
		//remove first element
		while(s.nextElement!=null){
			if(s.head==true){
				s=s.nextElement.nextElement;
			}
			s=s.nextElement;
		}
		
		//Print out elements of the list
		while(s.nextElement!=null){
			System.out.println(s.nextElement.value);
			s=s.nextElement;
		}
		//Add to the end of the list. Will have to be done using recursion
		int my_int = 5;
		while(true){
			if(s.nextElement==null){
				s.nextElement = new SnakeList();
				s.nextElement.head=false;
				s.nextElement.value=my_int;
				break;
			}
			s=s.nextElement;
			
		}
		
		//Create a list using only one element
		//This can be done using a loop
		s.nextElement = new SnakeList();
		s.nextElement.nextElement = new SnakeList();
	}

}
