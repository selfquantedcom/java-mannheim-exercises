//Friedl, 1571906
//2a)
public class SnakeList {
	boolean head;
	int value;
	SnakeList nextElement;

	public SnakeList() {
		head = true;
	}

	// 2b) Print out elements of the list
	public String toString() {
		String result = "";

		if (nextElement == null) {
			return "empty";
		}
		while (nextElement != null) {
			result += Integer.toString(nextElement.value);
			if (nextElement.nextElement != null) {
				result += ", ";
			}
			nextElement = nextElement.nextElement;
		}

		return result;
	}

	// 2c) append value
	public void add(int value) {
		if (nextElement == null) {
			SnakeList s = new SnakeList();
			s.value = value;
			s.head = false;
			s.nextElement = null;
			nextElement = s;
		} else {
			SnakeList next = nextElement;
			next.add(value);

		}

	}

	// 2d)
	public void remove(int value) {
		if (nextElement != null) {
			if (nextElement.value == value) {
				if (nextElement.nextElement != null) {
					nextElement = nextElement.nextElement;
				} else {
					nextElement = null;

				}

			} else {
				SnakeList next = nextElement;
				next.remove(value);
			}

		} else {
			System.out.println("value not found");
		}

	}

	// 2e)
	public static void main(String[] args) {
		SnakeList s = new SnakeList();
		s.add(1);
		s.add(5);
		s.add(7);
		s.add(1);
		s.add(9);
		s.add(0);
		s.add(6);
		
		System.out.println(s);
			
		SnakeList s1 = new SnakeList();
		s1.add(1);
		s1.add(5);
		s1.add(7);
		s1.add(1);
		s1.add(9);
		s1.add(0);
		s1.add(6);
		
		s1.remove(1);
		System.out.println(s1);
		//removes the first 1 rather than the fourth digit 1 
		
	}
}

