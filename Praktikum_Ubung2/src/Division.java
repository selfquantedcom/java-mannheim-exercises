
public class Division {
	public static void main(String[] args) {

		int a1 = 10 / 3;
		double a2 = 10 / 3;

		// int b1=10/3.0; darf keine Ganzezahl sein
		double b2 = 10 / 3.0;

		int c1 = 10 % 3;
		double c2 = 10 % 3;

		System.out.println("a1 " + a1);
		System.out.println("a2 " + a2);
		System.out.println("b2 " + b2);
		System.out.println("c1 " + c1);
		System.out.println("c2 " + c2);
	}

}
