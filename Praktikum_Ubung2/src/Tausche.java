
public class Tausche {
	public static void main(String[] args) {
		
		int a = IOTools.readInteger("Die erste ganze Zahl a: ");
		int b = IOTools.readInteger("Die zweite ganze Zahl b: ");
		
		int a1 = b;
		int b1 = a; 
		
		a = a1;
		b = b1; 
		
		System.out.println("Die Zahl a ist jetzt: " + a);
		System.out.println("Die Zahl b ist jetzt: " + b);
		
	}

}
