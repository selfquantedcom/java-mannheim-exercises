
public class Aufgabe_8 {
	public static void main(String[] args) {

		int jahr1 = IOTools.readInteger("Die Zahl der Menschen in dem ersten Jahr: ");
		int jahr2 = IOTools.readInteger("Die Zahl der Menschen in dem zweiten Jahr: ");

		double ergebnis = 100 * jahr2 / jahr1 - 100;
				
		System.out.println(ergebnis + "%");
	}

}
