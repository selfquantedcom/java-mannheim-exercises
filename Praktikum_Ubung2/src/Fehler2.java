
public class Fehler2 {
	public static void main(String[] argumente) {
		char c = 'A' * '1'; //WHAT DOES THIS DO?
		System.out.println("c = " + c);
		long i = 1000000000; // capacity issue, change to long
		System.out.println("i = " + i);
		System.out.println("2*i = " + (i + i));
		System.out.println("3*i = " + (i + i + i));
		System.out.println("4*i = " + (i + i + i + i)); //add brackets

	}
}
