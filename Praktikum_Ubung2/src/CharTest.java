
public class CharTest {
	public static void main(String[] args) {
		// a)
		// char c = 97; // a = 97, * = 42, b = 98, / = 47, c = 99;
		// System.out.print(c);
		// c = 42;
		// System.out.print(c);
		// c = 98;
		// System.out.print(c);
		// c = 47;
		// System.out.print(c);
		// c = 99;
		// System.out.print(c);

		// b)
		int i = 97;
		System.out.print((char) i);
		i = 42;
		System.out.print((char) i);
		i = 98;
		System.out.print((char) i);
		i = 47;
		System.out.print((char) i);
		i = 99;
		System.out.print((char) i);

	}

}
