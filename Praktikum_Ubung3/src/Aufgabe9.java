
public class Aufgabe9 {

	public static void main(String[] args) {
		int n = IOTools.readInteger("Geben Sie eine Ganzahl ein: ");
		// iteration variables
		int i = 1;
		int j = 3;
		// cumulative variables
		double pi = 0;
		double sum = 0;

		while (i <= n) {
			if (i % 2 != 0) {
				sum = sum - 1 / j;
				System.out.println(sum);
				i++;
				j = j + 2;
				System.out.println(sum);
			} else {
				sum = sum + 1 / j;
				i++;
				j = j + 2;
				System.out.println(sum);
			}
			//pi = 1 + 4 * sum;
			//System.out.println(pi);
		}
	}

}
