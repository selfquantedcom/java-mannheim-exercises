
public class Aufgabe5 {

	public static void main(String[] args) {
		
		int number = IOTools.readInteger("Geben Sie eine Zahl ein: ");
		int digit = 0; 
		int i = 0;
		int sum1 = 0;
		int sum2 = 0;
		
		while (number>0){
			digit = number%10;
			number = number/10;
			i = i+1;
			if (i%2==0){
				sum1 = sum1 - digit;				
			}
			else{
				sum2 = sum2 + digit;
			}
		}
		String result = (sum1+sum2==0|(sum1+sum2)%11==0)? "Die Zahl kann durch 11 dividierd werden" : "Die Zahl kann nicht durch 11 dividierd werden";
		System.out.println(result);
	}
		
}


