 
public class Aufgabe2 { 
	public static void main (String [] args) { 
		
		int x = 0, y = 4, x1 = 0, y1 = 0;
		
		//Beispiel A
		if (x < 5) 
			if (x < 0) 
				System.out.println("x<0"); 
		else 
			System.out.println("x>=5"); 
		//OK!????
		
		// Beispiel B 
		if (x > 0) { //Klammern! Im anderen fall wuerde die Bedingung nur fuer die ersten Zeile gueltig!
			System.out.println("ok! x>0"); 
			System.out.println("1/x=" + (1/x)); }
		//OK!?????
		
		// Beispiel C 
		if (x > 0) //Der Semi-kolom ermoeglicht nicht die folgende Bedingung. 
			System.out.println("1/x=" + (1/x)); 
			
		// Beispiel D 
		if (y > x) { //vertausche x und y
			x1 = x; //eine zwischen Variable wird gebraucht. 
			y1 = y;
			x = y1;
			y = x1;
			} 
		System.out.println("x=" + x + "y=" + y); 
		} 
	}

