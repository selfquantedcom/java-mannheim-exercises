
public class Aufgabe4 {

	public static void main(String[] args) {
		int zahl = (int) Math.floor(Math.random() * 99.0);
		int i = 1;

		System.out.println("Ich habe mir eine Zahl zwischen 0 und 99 ausgedacht.");
		int input = IOTools.readInteger("Rate->");

		while (input != zahl) {
			String response = (zahl > input) ? "Falsch! Meine Zahl ist groesser." : "Falsch! Meine Zahl ist kleiner";
			System.out.println(response);
			i = i + 1;
			input = IOTools.readInteger("Rate->");
		}
		System.out.println("Richtig! Du hast die Zahl mit " + i + "x Raten gefunden.");
	}

}

// Die optimale Strategie ist immer in der Mitte (d.h. der erste Rat is 49 denn
// es die Mitte zwischen 0,99 ist) zu raten. Das Ergebnis muss hoechstens in dem
// 7ten Rat geraten werden.