//Frantisek Friedl, 1571906

package de.pi4.snake;

//2a)
public class Field {
	public int X;
	public int Y;
	public boolean isApple = false;

	public Field(int x, int y) {
		
		this.isApple = false;
		X = x;
		Y = y;

	}

}
