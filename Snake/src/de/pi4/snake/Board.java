//Frantisek Friedl, 1571906

package de.pi4.snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * This class represents the Arena with the snakes
 * 
 * @author dschoen
 *
 */
public class Board extends JPanel {

	private static final long serialVersionUID = -8879687040738022049L;

	private Snake game;
	
	// array and LinkedList
	public static Field[][] board = new Field[20][20];
	LinkedList<Field> snake = new LinkedList<>();
	
	//initial snake coordinates
	public final int[][] snakeStartPosition = { {0, 0}, {1, 0}, {2, 0}};

	// --- Constructor -------------------------------------------------

	public Board(Snake game) {
		this.game = game;
		// 2b
		
		// fill in array with Fields
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				board[i][j] = new Field(i, j);
			}
		}
		
		// 2c)
		// default snake position - fields		
		setDefaultSnakePosition();
		
		// set an initial apple 
		setApple();
		
		
		
	}
	//initial snake fields
	private void setDefaultSnakePosition() {
		snake.add(new Field(snakeStartPosition[0][0], snakeStartPosition[0][1]));
		snake.add(new Field(snakeStartPosition[1][0], snakeStartPosition[1][1]));
		snake.add(new Field(snakeStartPosition[2][0], snakeStartPosition[2][1]));
	}

	// --- Methods -------------------------------------------------
	
	/**
	 * paints everything
	 */
	@Override
	public void paint(Graphics g) {
		// cleans up screen
		super.paint(g);

		// set up graphics
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// draw board
		this.paintBoard(g2d);

		// move snake
		this.moveSnake();

		// draw snakes
		this.paintSnake(g2d);
	}

	//setApple method 
	private void setApple() {
		
		//don't place apple if snake exists there
		boolean collisionWithSnake = true;
		int appleX = -1;
		int appleY = -1;
		
		while(collisionWithSnake){
			appleX = (int) (Math.random() * 20);
			appleY = (int) (Math.random() * 20);
			
			collisionWithSnake = checkSnakeCollision(appleX, appleY);
		}
		//implement apple
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (i == appleX && j == appleY) {
					board[i][j].isApple = true;
				} else {
					board[i][j].isApple = false;
				}
			}
		}

	}
	
	private boolean checkSnakeCollision(int appleX, int appleY){
		boolean collision = false;
		
		//TODO implement
		
		return collision;
	}

	/**
	 * Draws the grid board
	 * 
	 * @param g
	 */
	private void paintBoard(Graphics2D g) {

		// draw the board
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				g.drawRect(20 * i, 20 * j, 20, 20);

				// draw apple
				if (board[i][j].isApple == true) {					
					paintApple(g, i, j);
				}

			}
		}

	}
	//use to draw the apple
	private void paintApple(Graphics2D g, int x, int y) {
		g.setColor(Color.red);
		g.fillOval(x * 20, 20 * y, 20, 20);
		g.setColor(Color.BLACK);
	}

	/**
	 * Draws the current snake position
	 * 
	 * @param g
	 */
	private void paintSnake(Graphics2D g) {

		// set snake color
		g.setColor(Color.blue);

		// draw snake
		for (Field field : snake) {
			g.fillOval(field.X * 20, field.Y * 20, 20, 20);
		}

	}

	/**
	 * Moves the snake over the board
	 */
	private void moveSnake() {
		// TODO implement snake movement
		
		//increase snake when apple is hit
		if (checkCollisionWithApple()) {
			growSnake();
		} else {
			//move left
			if (game.snakeDirecton == Snake.LEFT) {
				snake.removeFirst();
				snake.addLast(new Field(snake.getLast().X - 1, snake.getLast().Y));
			}
			// move right
			if (game.snakeDirecton == Snake.RIGHT) {
				snake.removeFirst();
				snake.addLast(new Field(snake.get(snake.size()-1).X + 1, snake.get(snake.size()-1).Y));
			}
			// move up
			if (game.snakeDirecton == Snake.UP) {
				snake.removeFirst();
				snake.add(new Field(snake.get(snake.size()-1).X, snake.get(snake.size()-1).Y - 1));
			}
			// move down
			if (game.snakeDirecton == Snake.DOWN) {
				snake.removeFirst();
				snake.add(new Field(snake.get(snake.size()-1).X, snake.get(snake.size()-1).Y + 1));
			}
		}
		
		
		

	}
	//increase snake when an apple is hit
	private void growSnake() {
		switch (game.snakeDirecton) {
		case 1:
			snake.addLast(new Field(snake.getLast().X - 1, snake.getLast().Y));
			break;
		case 2:
			snake.addLast(new Field(snake.getLast().X, snake.getLast().Y - 1));
			break;
		case 3:
			snake.addLast(new Field(snake.getLast().X + 1, snake.getLast().Y));
			break;
		case 4:
			snake.addLast(new Field(snake.getLast().X, snake.getLast().Y + 1));
			break;
		default:
			break;
		}
		//generate new apple
		setApple();
	}
	
	//check if apple is hit
	private boolean checkCollisionWithApple() {
		boolean collision = false;
		
		if(board[snake.getLast().X][snake.getLast().Y].isApple){
			collision = true;
		}
		
		return collision;
	}


}
