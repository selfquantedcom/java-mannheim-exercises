package de.pi4.snake.io;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.pi4.snake.Board;
import de.pi4.snake.Snake;

/**
 * The Window Class represents the full window on the screen.
 * It renders all appearances.
 * 
 * @author dschoen
 *
 */
public class Window extends JFrame {

	private static final long serialVersionUID = -4535052202864274946L;
	
	private InputListener listener;
	private JPanel boardPanel;
	
	private Snake game;
	
	public Window(Snake game, String title) {
		super(title);
		this.game = game;
		
		// Create Window frame
		this.setSize(500,500);
		this.setLayout(new BorderLayout());
			
		// create Arena (BoardLayout)
		this.boardPanel = new Board(game);
		
		// Adds Arena Panel to Window frame
		this.add(this.boardPanel, BorderLayout.CENTER);
		
		// register key listener
		this.listener = new InputListener(this.game);
		this.addKeyListener(listener);
		this.setFocusable(true);
		
		// set visibility and stuff
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void update(){
		this.boardPanel.repaint();	
	}
}
