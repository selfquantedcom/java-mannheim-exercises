package de.pi4.snake.io;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import de.pi4.snake.Snake;

public class InputListener implements KeyListener {

	Snake game;
	
	// --- Constructor -----------------------------------------------------
	
	public InputListener(Snake game) {
		this.game = game;
	}
	
	// --- Methods ----------------------------------------------
	
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		//System.out.println("Key: "+KeyEvent.getKeyText(e.getKeyCode())+" Code: "+e.getKeyCode());
		
		switch(e.getKeyCode()) {
			case 37:	// left
				game.snakeDirecton = Snake.LEFT;
			break;
			case 38:	// up
				game.snakeDirecton = Snake.UP;
			break;
			case 39:	// right
				game.snakeDirecton = Snake.RIGHT;
			break;
			case 40:	// down
				game.snakeDirecton = Snake.DOWN;
			break;
			default:
				System.out.println("Unknown Key: "+KeyEvent.getKeyText(e.getKeyCode())+" Code: "+e.getKeyCode());
		}		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// do nothing
	}
}
