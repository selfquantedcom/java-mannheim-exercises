// Friedl Frantisek, 1571906
public class Numbers {
	/**
	 * Rundet eine Gleitkommazahl gro�z�gig auf bzw. ab.
	 * 
	 * @param number
	 *            Die Eingabe als Gleitkommazahl.
	 * @return Die gerundete Eingabezahl wird als ganze Zahl zur�ckgegeben.
	 */
	static int roundLavishly(double number) {

		int temp = (int) number;
		if (number - temp < 0.3) {
			return (int) number;
		} else {
			return (int) number + 1;
		}

	}

	/**
	 * Rundet eine Gleitkommazahl gro�z�gig auf ein Vielfaches einer gegebenen
	 * Schrittweite. Beispiel 1: Zahl = 113.1, Schrittweite = 10: Ergebnis =
	 * 120. Beispiel 2: Zahl = 27.5, Schrittweite = 5: Ergebnis = 30. Beispiel
	 * 3: Zahl = 25.9, Schrittweite = 5: Ergebnis = 25.
	 * 
	 * Wenn die Schrittweite beispielsweise 10 ist, wird auf den n�chsten
	 * 10er-Schritt gerundet, also 112.0 auf 110, 27.8 auf 30, 210.9 auf 210,
	 * usw.
	 * 
	 * @param number
	 *            Die Eingabe als Gleitkommazahl.
	 * @param step
	 *            Die Schrittweite, auf deren Vielfache gerundet wird.
	 * @return
	 */
	static int roundTo(double number, int step) {
		int result = step * roundLavishly(number / step);
		return result;
	}

	/**
	 * Schneidet die letzten n Stellen einer Zahl ab. Beispiel 1: Number =
	 * 1234567, n = 3: Ergebnis = 1234. Beispiel 2: Number = 1234567, n = 5:
	 * Ergebnis = 12.
	 * 
	 * @param number
	 *            Eine ganze Zahl, von der abgeschnitten wird.
	 * @param digits
	 *            Dies ist die Anzahl n an Stellen, die abgeschnitten werden.
	 * @return Das Ergebnis wird von der Methode wieder als ganze Zahl
	 *         zur�ckgegeben.
	 */
	static int cut(int number, int digits) {
		int i = 0;
		while (i < digits) {
			number = number / 10;
			i++;
		}

		return number;
	}

	public static void main(String[] args) {
		System.out.println("Aufrunden:");
		System.out.println(roundLavishly(3.14));
		System.out.println(roundLavishly(10.5));
		System.out.println(roundLavishly(0.49));

		System.out.println("Runde zu gegebender Schrittweite:");
		System.out.println(roundTo(123.3, 10));
		System.out.println(roundTo(22.3, 8));
		System.out.println(roundTo(27.5, 5));

		System.out.println("Letzte n Zahlen der Matrikelnummer abschneiden:");
		System.out.println(cut(1571906, 3));
		System.out.println(cut(1571906, 5));
	}
}
