// Friedl Frantisek, 1571906
public class Boolean {

	public static boolean magic(boolean x, boolean y, boolean z) {

		return x & !y | z;
	}

	public static void main(String[] args) {

		System.out.println("  x\t y\t z\t -> magic");
		System.out.println("  ------------------------------");

		for (byte n = 0; n < 8; n++) {
			byte mask = 1;
			boolean x = (n & mask << 2) > 0;
			boolean y = (n & mask << 1) > 0;
			boolean z = (n & mask << 0) > 0;

			boolean res = magic(x, y, z);

			System.out.println("  " + x + "\t " + y + "\t " + z + "\t -> " + res);
		}
	}
}
// I assumed that we will need all the three letters to produce the result.
// Hence we have x,y,z. To produce the final result we will essentially need to
// compare two expressions with each other.In the first case when all variables
//are equal to false, the result is false. We try to produce a matching solution 
//in the simplest way possible which means trying F|F as this is the only
//combination in the particular table producing false. From this we the simplest
//possibility is to add the last variable getting x&y|z. 
//This produces almost a perfect match with the exception of two lines 
//which need to be swapped. This suggests that we are close to the correct
//solution. Experimentation with small changes
// in the setting then leads us to the correct solution.
