class Punkt {
	public double x;
	public double y;
}

class Strecke {
	public Punkt from;
	public Punkt to;
}

public class Form {
	public Strecke[] streckenzug;

	public static void main(String[] args) {
		Punkt p1 = new Punkt();
		p1.x = 0;
		p1.y = 0;
		Punkt p2 = new Punkt();
		p2.x = 5;
		p2.y = 0;
		Punkt p3 = new Punkt();
		p3.x = 5;
		p3.y = 5;
		Punkt p4 = new Punkt();
		p4.x = 0;
		p4.y = 5;

		Strecke s1 = new Strecke();
		s1.from = p1;
		s1.to = p2;
		Strecke s2 = new Strecke();
		s2.from = p2;
		s2.to = p3;
		Strecke s3 = new Strecke();
		s3.from = p3;
		s3.to = p4;
		Strecke s4 = new Strecke();
		s4.from = p4;
		s4.to = p1;
		Form form = new Form();
		form.streckenzug = new Strecke[] { s1, s2, s3, s4 }; 
		
		// *** hier ***
		form.streckenzug[1].to.y = 3;
		form.streckenzug[3].from.y = 3; 
		// *** und hier ***
	}
}
