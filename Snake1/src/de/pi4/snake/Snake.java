package de.pi4.snake;

import de.pi4.snake.io.Window;

/**
 * Main Class.
 * 
 * @author Daniel Sch�n
 *
 */
public class Snake {
		
	// the window representing the game
	private Window gameWindow; 
	
	// Markers for the direction
	public static final int LEFT 	= 1;
	public static final int UP 		= 2;
	public static final int RIGHT 	= 3;
	public static final int DOWN 	= 4;
	
	// Direction of the Snake
	public int snakeDirecton = Snake.RIGHT;
	
	boolean isRunning 	= true;
	
	// This game
	public Snake game;
	
	// --- Main Method --------------------------------------------------------------
	
	public static void main(String[] args) throws InterruptedException {

		// Create Game Object	
		Snake game = new Snake();
		game.game = game;		// funny line ;-)	
		
		// Set Up Graphics & Layout
		game.gameWindow = new Window(game, "Snake");
	
     
        // --- Game Loop ---
        while (game.isRunning) {        
        	  	
        	// render Graphics
        	game.gameWindow.update();
        	
        	// wait for 200 ms
        	// set this to whatever speed you like
        	// a higher number means a slower game
			Thread.sleep(200);			
		}
    }

	// -------------------------------------------------------------------

}
