package de.pi4.snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * This class represents the Arena with the snakes
 * 
 * @author dschoen
 *
 */
public class Board extends JPanel {

	private static final long serialVersionUID = -8879687040738022049L;

	private Snake game;

	// array and LinkedList
	public static Field[][] array = new Field[20][20];
	LinkedList<Field> snake = new LinkedList<Field>();
	
	// default snake fields
	Field f1 = new Field(0, 0);
	Field f2 = new Field(1, 0);
	Field f3 = new Field(2, 0);

	// --- Constructor -------------------------------------------------

	public Board(Snake game) {
		this.game = game;
		// 2b
		
		// fill in array with Fields
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				array[i][j] = new Field(i, j);
				

			}
		}
		// set an apple
		setApple();
		
				
		// 2c)
		// default snake
		snake.add(f1);
		snake.add(f2);
		snake.add(f3);
		
	}

	// --- Methods -------------------------------------------------

	/**
	 * paints everything
	 */
	@Override
	public void paint(Graphics g) {
		// cleans up screen
		super.paint(g);

		// set up graphics
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// draw board
		this.paintBoard(g2d);

		// move snake
		this.moveSnake();

		// draw snakes
		this.paintSnake(g2d);
	}

	//setApple method 
	private void setApple() {
		int x = (int) (Math.random() * 20);
		int y = (int) (Math.random() * 20);
		
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				array[i][j].apple=false;
				
				if (i == x && j == y) {
					array[i][j].apple  = true;
				}
			}
		}

	}

	/**
	 * Draws the grid board
	 * 
	 * @param g
	 */
	private void paintBoard(Graphics2D g) {

		// draw the board
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				g.drawRect(20 * i, 20 * j, 20, 20);

				// check for an apple and draw it
				if (array[i][j].apple == true) {					
					g.setColor(Color.red);
					g.fillOval(i * 20, 20 * j, 20, 20);
					g.setColor(Color.BLACK);
					
					
				}

			}
		}

	}

	/**
	 * Draws the current snake position
	 * 
	 * @param g
	 */
	private void paintSnake(Graphics2D g) {

		// set snake color
		g.setColor(Color.blue);

		// draw the initial snake
		g.fillOval(20, 0, 20, 20);
		g.fillOval(40, 0, 20, 20);
		g.fillOval(0, 0, 20, 20);

	}

	/**
	 * Moves the snake over the board
	 */
	private void moveSnake() {
		// TODO implement snake movement

		// move left
		if (game.snakeDirecton == 1) {
			snake.removeLast();
			snake.addFirst(new Field(f1.X - 1, f1.Y));
		}
		// move right
		if (game.snakeDirecton == 3) {
			snake.removeFirst();
			snake.addLast(new Field(f3.X + 1, f3.Y));
		}
		// move up
		if (game.snakeDirecton == 2) {
			snake.removeFirst();
			snake.add(new Field(f3.X, f3.Y + 1));
		}
		// move down
		if (game.snakeDirecton == 3) {
			snake.removeFirst();
			snake.add(new Field(f3.X, f3.Y - 1));
		}
		// check for Apple
		if (snake.getLast().apple == true) {
			snake.addFirst(new Field(f1.X - 1, f1.Y));
			snake.getLast().apple=false;
			setApple();
		}

	}

}
