// Friedl, 1571906
public class Painter {
	static void drawBackground() {
		Square sq = new Square();
		sq.changeColor("black");
		sq.changeSize(300);
		sq.moveHorizontal(-60);
		sq.slowMoveVertical(-40);
		sq.makeVisible();

	}

	static void drawStar(int x, int y) {
		Triangle tri = new Triangle();
		tri.changeSize(15, 20);
		tri.changeColor("yellow");
		tri.moveVertical(y);
		tri.moveHorizontal(x);
		tri.makeVisible();

		Triangle tri1 = new Triangle();
		tri1.changeColor("yellow");
		tri1.makeVisible();
		tri1.changeSize(-15, 20);
		tri1.moveVertical(y + 20);
		tri1.moveHorizontal(x);

	}

	public static void main(String[] args) {
		drawBackground();

		for (int x = 10; x <= 190; x = x + 10) {
			for (int y = 20; y <= 170; y = y + 30) {
				if (y == 20 | y == 80 | y == 140) {
					switch (x) {
					case 10:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;
					case 80:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;
					case 150:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;
					}
				} else {
					switch (x) {
					case 50:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;

					case 120:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;
					case 190:
						drawStar(x, y);
						drawStar(x, y);
						drawStar(x, y);
						break;
					}
				}
			}
		}
	}
}
