//// Generated from Lexer12.g4 by ANTLR 4.2.2
//
//import java.io.*;
//import org.antlr.v4.runtime.*;
//
//import org.antlr.v4.runtime.Lexer;
//import org.antlr.v4.runtime.CharStream;
//import org.antlr.v4.runtime.Token;
//import org.antlr.v4.runtime.TokenStream;
//import org.antlr.v4.runtime.*;
//import org.antlr.v4.runtime.atn.*;
//import org.antlr.v4.runtime.dfa.DFA;
//import org.antlr.v4.runtime.misc.*;
//
//@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
//public class Lexer12 extends Lexer {
//	protected static final DFA[] _decisionToDFA;
//	protected static final PredictionContextCache _sharedContextCache =
//		new PredictionContextCache();
//	public static final int
//		INTEGER=1, RATIONAL=2, IF=3, IDENTIFIER=4, SKIP=5;
//	public static String[] modeNames = {
//		"DEFAULT_MODE"
//	};
//
//	public static final String[] tokenNames = {
//		"<INVALID>",
//		"INTEGER", "RATIONAL", "'if'", "IDENTIFIER", "SKIP"
//	};
//	public static final String[] ruleNames = {
//		"LETTER", "DIGIT", "DIGITS", "INTEGER", "RATIONAL", "IF", "IDENTIFIER", 
//		"WS", "SKIP"
//	};
//
//
//	    public static void main(String[] args) throws IOException {
//	        Lexer12 lexer = new Lexer12(new ANTLRInputStream(System.in));
//	        CommonTokenStream tokens = new CommonTokenStream(lexer);
//	        tokens.fill();
//	        System.out.println("\nTokens:");
//	        for (Token token : tokens.getTokens()) {
//	            if (token.getType() == -1) continue;
//	            System.out.println("<" + lexer.getTokenNames()[token.getType()]
//	                + ", '" + token.getText() + "'>");
//	       }
//	    }
//
//
//	public Lexer12(CharStream input) {
//		super(input);
//		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
//	}
//
//	@Override
//	public String getGrammarFileName() { return "Lexer12.g4"; }
//
//	@Override
//	public String[] getTokenNames() { return tokenNames; }
//
//	@Override
//	public String[] getRuleNames() { return ruleNames; }
//
//	@Override
//	public String getSerializedATN() { return _serializedATN; }
//
//	@Override
//	public String[] getModeNames() { return modeNames; }
//
//	@Override
//	public ATN getATN() { return _ATN; }
//
//	public static final String _serializedATN =
//		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\79\b\1\4\2\t\2\4"+
//		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2"+
//		"\3\3\3\3\3\4\6\4\33\n\4\r\4\16\4\34\3\5\5\5 \n\5\3\5\3\5\3\6\3\6\3\6\5"+
//		"\6\'\n\6\3\7\3\7\3\7\3\b\3\b\3\b\7\b/\n\b\f\b\16\b\62\13\b\3\t\3\t\3\n"+
//		"\3\n\3\n\3\n\2\2\13\3\2\5\2\7\2\t\3\13\4\r\5\17\6\21\2\23\7\3\2\5\3\2"+
//		"c|\3\2\62;\5\2\13\f\17\17\"\"9\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2"+
//		"\17\3\2\2\2\2\23\3\2\2\2\3\25\3\2\2\2\5\27\3\2\2\2\7\32\3\2\2\2\t\37\3"+
//		"\2\2\2\13#\3\2\2\2\r(\3\2\2\2\17+\3\2\2\2\21\63\3\2\2\2\23\65\3\2\2\2"+
//		"\25\26\t\2\2\2\26\4\3\2\2\2\27\30\t\3\2\2\30\6\3\2\2\2\31\33\5\5\3\2\32"+
//		"\31\3\2\2\2\33\34\3\2\2\2\34\32\3\2\2\2\34\35\3\2\2\2\35\b\3\2\2\2\36"+
//		" \7/\2\2\37\36\3\2\2\2\37 \3\2\2\2 !\3\2\2\2!\"\5\7\4\2\"\n\3\2\2\2#&"+
//		"\5\t\5\2$%\7\60\2\2%\'\5\7\4\2&$\3\2\2\2&\'\3\2\2\2\'\f\3\2\2\2()\7k\2"+
//		"\2)*\7h\2\2*\16\3\2\2\2+\60\5\3\2\2,/\5\3\2\2-/\5\5\3\2.,\3\2\2\2.-\3"+
//		"\2\2\2/\62\3\2\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\20\3\2\2\2\62\60\3\2\2"+
//		"\2\63\64\t\4\2\2\64\22\3\2\2\2\65\66\5\21\t\2\66\67\3\2\2\2\678\b\n\2"+
//		"\28\24\3\2\2\2\b\2\34\37&.\60\3\b\2\2";
//	public static final ATN _ATN =
//		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
//	static {
//		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
//		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
//			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
//		}
//	}
//}