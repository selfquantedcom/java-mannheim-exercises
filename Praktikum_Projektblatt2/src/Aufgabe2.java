// Friedl Frantisek, 1571906
public class Aufgabe2 {

	static int N = 15;

	/* *** Aufgabenteil (a) *** */
	public static void folge_a1() {
		int i = 0;
		while (i < N) {
			int resultat = 3 * i;
			i = i + 1;
			System.out.println(resultat);
		}

	}

	public static void folge_a2() {
		int i = 0;
		while (i < N) {
			if (i == 1) {
				System.out.println(3);
				i = i + 1;
			} else {
				int resultat = 3 * (i - 1) + 3;
				i = i + 1;
				System.out.println(resultat);

			}
		}
	}

	/* *** Aufgabenteil (b) *** */
	public static void folge_b() {
		int i = 0;
		int resultat = 0;
		while (i < N) {
			resultat = resultat + 2 * i;
			i++;
			System.out.println(resultat);
		}

	}

	/* *** Aufgabenteil (c) *** */
	public static void folge_c() {
		int i = 1;
		double resultat = 1.5;
		
		System.out.println("a0= " + 1.5);
		while (i < N) {
			if (i % 2 == 0) {
				resultat = resultat - 1;
				i++;
				System.out.println(resultat);
			} else {
				resultat = resultat*2;
				i++;
				System.out.println(resultat);
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("Folge (a1):");
		Aufgabe2.folge_a1();
		System.out.println("Folge (a2):");
		Aufgabe2.folge_a2();
		System.out.println("Folge (b):");
		Aufgabe2.folge_b();
		System.out.println("Folge (c):");
		Aufgabe2.folge_c();
	}

}
