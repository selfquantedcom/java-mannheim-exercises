// Friedl Frantisek, 1571906
public class Aufgabe3 {
	public static void main(String[] args) {
		// Define variables
		int a = IOTools.readInteger("Integer a: ");
		int b = IOTools.readInteger("Integer b: ");
		int sum1 = 0;
		int sum2 = 0;
		int i = 1;
		// Gebe die Summe der Teiler von a aus
		while (i < a) {
			if (a % i == 0) {
				sum1 = sum1 + i;
				i = i + 1;
			} else {
				i = i + 1;
			}
		}
		// Gebe die Summe der Teiler von b aus
		while (i < b) {
			if (a % i == 0) {
				sum2 = sum2 + i;
				i = i + 1;
			} else {
				i = i + 1;
			}
		}
		// Pruefe ub die Zahlen befreundet sind
		String resultat = (a==sum2 & b==sum1)? "Zahlen sind befreundet":"Zahlen sind nicht befreundet";
		System.out.println(resultat);
	}

}
