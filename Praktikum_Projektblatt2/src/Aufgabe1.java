// Friedl Frantisek, 1571906
//1a)
//public class Aufgabe1 {
//	public static void main(String[] args) {
//		int n = IOTools.readInteger("n=");
//		int z = n;
//		while (n < 0) {
//			z += 2;
//			n++;
//			
//		}
//		System.out.println("z=" + z);
// Der Algorithmus gibt die positive (bzw. non-negative) Indentiataet einer Nummer aus. 
// z=|n|
//	}

//}

//1b)
//public class Aufgabe1 {
//
//	public static void main(String[] args) {
//		int n = IOTools.readInteger("n=");
//		int z = 1;
//		for (int i = 1; i <= n; i++) {
//			z = -z;
//		}
//		System.out.println("z=" + z);
//		// Das Ergebnis ist -1 fuer eine ungerade Nummer und +1 fuer eine gerade
//		// Nummer. z=-z fuer n%2!=0, z=z fuer n%2==0
//		
//	}
//}

//1 c)
//public class Aufgabe1 {
//	public static void main(String[] args) {
//		int a = IOTools.readInteger("a=");
//		int b = IOTools.readInteger("b=");
//		int z = 0;
//		for (int i = 1; i <= a; i++) {
//			for (int j = 1; j <= b; j++) {
//				z++;
//
//			}
//		}
//		System.out.println("z=" + z);
//	}
//}
////Multiplikation fuer positive Zahlen. 0 wird ausgedrueckt wenn eine
//// negative Zahl eingegeben wird. z=a*b wo a&b>=0


//1d)
//public class Aufgabe1 {
//	public static void main(String[] args) {
//		int a = IOTools.readInteger("a=");
//		int b = IOTools.readInteger("b=");
//		int z = 0;
//		while (a >= b) {
//			for (int i = 1; i <= b; i++) {
//				a--;
//			}
//			z++;
//		}
//		int r = a;
//		System.out.println("z=" + z + " r=" + r);
//		// Divion + remainder a/b und a%b
		// z = a/b, r = a%b
//	}
//}

//1e)
//public class Aufgabe1 {
//	public static void main(String[] args) {
//		int a = IOTools.readInteger("a=");
//		int b = IOTools.readInteger("b=");
//		int z = 0;
//		while (a != b) {
//			if (a > b) {
//				a--;
//				z++;
//			} else {
//				a++;
//				z--;
//			}
//		}
//		z += a;
//		System.out.println("(e) z=" + z);
//		// z=a
//	}
//}
