//Frantisek Friedl, 1571906

// Jahr hat 31536000 Sekunden
// Tag hat 86400 Sekunden
// Stunde hat 3600 Sekunden

public class Aufgabe_2 {
	public static void main(String[] args) {

		int Sekunden = IOTools.readInteger("Anzahl Sekunden eingeben:");

		int Jahre = Sekunden / 31536000;
		int Tage = (Sekunden % 31536000) / 86400;
		int Stunden = ((Sekunden % 31536000) % 86400) / 3600;
		int Minuten = (((Sekunden % 31536000) % 86400) % 3600) / 60;
		int Rest = (((Sekunden % 31536000) % 86400) % 3600) % 60;
		System.out.println("Das entspricht:");
		System.out.println(Jahre + " Jahren");
		System.out.println(Tage + " Tagen");
		System.out.println(Stunden + " Stunden");
		System.out.println(Minuten + " Minuten und");
		System.out.println(Rest + " Sekunden");
	}

}
