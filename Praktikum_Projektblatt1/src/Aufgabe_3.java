//Frantisek Friedl, 1571906
public class Aufgabe_3 {
	public static void main(String[] args) {

		int x = IOTools.readInteger("Geben sie ein Jahr, zwischen 1900-2009 (inklusive), ein! : ");

		float a = ((x - 1900) % 19);
		float b = ((7 * a + 1) / 19);
		float m = ((11 * a + 4 - b) % 29);
		float q = ((x - 1900) / 4);
		float w = ((x - 1900 + q + 31 - m) % 7);
		float t = 25 - m - w;
		float temp = 31 + t;

		String Ergebnis = (t > 0) ? ("Ostersonntag, " + Math.round(t) + ". April " + x)
				: ("Ostersonntag, " + Math.round(temp) + ". Maerz " + x);
		System.out.println(Ergebnis);

	}

}
