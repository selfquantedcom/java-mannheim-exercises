//Frantisek Friedl, 1571906

public class Aufgabe_1a {
	public static void main(String[] args) {
		
		double a = 23.5; // kein integer, sondern double
		
		double b = 3.14; // kein float, sondern double oder explitzite Typeumwandlung in float
		
		double c = (byte)a ^ 2 + (byte)b ^ 2; // Operator ^ wird fuer byte Typ definiert
		
		boolean d = a > (b / c) - 99; // type boolean
		
		String C = "A"; // type string
		
		String s = (a == 0) ? "gleich_nul" : "ungleich_Null"; 	// ";" stadt ":" am Ende
																// "==" stadt "="
																
		System.out.println("Dies ist ein Test"); // "" fehlen fuer den Text

	}
}
