//Frantisek Friedl, 1571906

public class Aufgabe_1b{
    public static void main(String[] args){
	double a = IOTools.readDouble("a = ");
 	double b = IOTools.readDouble("b = ");
	double c = IOTools.readDouble("c = ");

	
	double wt = Math.sqrt(b * b - 4 * a * c);

	//Problem gibt es auf der naechsten Codezeile die lautete: double lsg1 = b + wt / 2 * a;  
	//Laut der quadratischen Formula braucht man das folgende (auf die Klammenr auch achten!):

	double lsg1 = (-b + wt) / (2 * a);  
	double lsg2 = -(b + wt) / 2 / a;  

	System.out.println("Loesung 1: " + lsg1);
	System.out.println("Loesung 2: " + lsg2);	
   }
}
//Ein Problem gibt es zusaetzlich wenn es kein Ergebnis gibt. Dafuer koennte man
//eine Bedingung benutzen (if/else) mit dem Resultat, dass es kein Ergebnis gibt
//(das waere vielleicht nutzerfreundlicher als "NaN")

