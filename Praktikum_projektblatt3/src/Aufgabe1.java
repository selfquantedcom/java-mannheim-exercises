//Friedl Frantisek, 1571906

public class Aufgabe1 {
	public static final int N = 500;
	public static final int[] primFeld = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
			73, 79, 83, 89, 97 }; // Feld mit Primzahlen bis 100

	public static int[] feld = new int[N]; // Feld fuer N Zahlen
	public static int[] result = new int[N]; // initiate a new array to be used
												// in test 2 and 3

	// fuellt feld mit N Zufallszahlen
	public static void fuellen() {
		for (int i = 0; i < N; i++) {
			feld[i] = (int) (Math.random() * 99 + 1);
		}
	}

	// Test, wie oft eine bestimmte Zahl im Feld vorkommt
	public static void test1() {
		int zahl = IOTools.readInteger("Zahl: ");
		int count = 0;

		for (int i = 0; i < N; i++) {
			if (feld[i] == zahl) {
				count = count + 1;
			}

		}
		System.out.println("The occurence of this number is: " + count);
	}

	// alle Primzahlen im Feld ausgeben (Mehrfachnennungen ok)
	public static void test2() {

		int k = 0; // in order to store elements of "feld" in "result" without
					// empty positions in between individual elements

		for (int i = 0; i < N; i++) { // iterate through "feld"
			for (int j = 0; j < primFeld.length; j++) { // iterate through
														// primFeld
				if (feld[i] == primFeld[j]) { // add the number to "result" if
												// prime
					result[k] = primFeld[j];
					k++;
				}
			}
		}
		// To print the result
		int format = 0;
		System.out.println("The prime numbers contained in 'Feld' are: ");
		for (int i = 0; i < result.length; i++) {
			if (result[i] == 0) { // eliminate unused spaces
				break;
			}
			// To print the prime numbers... somewhat nicely :)
			System.out.print(result[i] + " ");
			format++;
			if (format == 20) {
				System.out.println();
				format = 0;
			}

		}

	}

	// Maximaler Wert fuer die Summe dreier aufeinanderfolgender Zahlen
	public static void test3() {
		int sum = 0;
		int k = 1;

		// to store the three numbers
		int number1 = 0;
		int number2 = 0;
		int number3 = 0;

		for (int i = 1; i < (N - 1); i++) {
			sum = feld[(i - 1)] + feld[i] + feld[(i + 1)];
			result[k] = sum;
			// if current sum is the largest, remember the three numbers
			if (result[k] > result[(k - 1)]) {
				number1 = feld[(i - 1)];
				number2 = feld[i];
				number3 = feld[(i + 1)];
				k++;
			}

		} // printout the three numbers and their sum
		System.out.print(number1 + number2 + number3 + " = ");
		System.out.print(number1 + " + ");
		System.out.print(number2 + " + ");
		System.out.print(number3);
	}

	public static void main(String[] args) {
		fuellen();
		// test1();
		// test2();
		// test3();
	}
}
