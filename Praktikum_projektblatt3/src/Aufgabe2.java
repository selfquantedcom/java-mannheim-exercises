//Friedl Frantisek, 1571906
public class Aufgabe2 {
	public static int[] zahlen = new int[9];

	// gibt Feld zahlen aus
	public static void printZahlen() {
		for (int i = 0; i < 9; i++) {
			System.out.print(zahlen[i] + " ");
		}
		System.out.println();
	}

	// fuellt Feld zahlen mit 1,2,...,9 in zufaelliger Reihenfolge
	public static void zufall() {
		for (int i = 1; i <= 9; i++) {
			zahlen[i - 1] = i;
		}
		for (int i = 1; i <= 50; i++) {
			// zufaellig zwei Indizes waehlen
			int m = (int) (Math.random() * 9); // Zufallszahl 0 - 8 (Index)
			int n = (int) (Math.random() * 9); // Zufallszahl 0 - 8 (Index)
			if (m != n) { // Elemente im Feld vertauschen
				int help = zahlen[m];
				zahlen[m] = zahlen[n];
				zahlen[n] = help;
			}
		}
	}

	public static void main(String[] args) {
		zufall();
		printZahlen();

		int[] my; // declare my array

		boolean ok = false;
		int count = 0;

		while (ok == false) { // "False" until the result is achieved

			int k = 0;

			// Step 1) keep asking for number until 1<=k<=9
			while (1 > k | k > 9) {
				k = IOTools.readInteger("Input a number between 1 and 9 (inclusive): ");
			}
			// put first "k" numbers into my array
			my = new int[k];
			for (int i = 0; i < k; i++) {
				my[i] = zahlen[i];
			}
			// Step 2) input numbers back from "my" into "zahlen" in reverse
			for (int i = 0; i < k; i++) {
				zahlen[i] = my[k - 1 - i];
			}
			// Step 3)
			printZahlen();
			count = count + 1;

			// is the array "zahlen" sorted? If not, repeat the whole process
			for (int i = 0; i < 9; i++) {
				if (zahlen[i] > zahlen[i + 1]) {
					ok = false;
					break;
				} else {
					ok = true;
				}
			}

		}

		System.out.println("Herzlichen Glueckwunsch! Sie haben " + count + "Schritte gebraucht.");
	}
}
