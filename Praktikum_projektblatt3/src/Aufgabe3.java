//Friedl Frantisek, 1571906
public class Aufgabe3 {
	// public final static String SCHLOSS = "uni-mannheim-schloss-bach2";
	public final static String SCHLOSS = "schloss_1";

	public static void teil_a() {
		String outputfile1 = "test_a1";
		String outputfile2 = "test_a2";
		int[][] data1;
		int[][] data2;
		int size = 256;

		// *********** hier einfuegen ***********

		// Define-initiate variables
		int increment = 1;
		data1 = new int[size][size];
		data2 = new int[size][size];

		// fill in data1
		for (int i = 0; i < 256; i++) {
			increment++;
			for (int j = 0; j < 256; j++) {
				data1[i][j] = increment;

			}
		}
		// fill in data2
		for (int i = 0; i < 256; i++) {
			increment = 1;
			for (int j = 0; j < 256; j++) {
				data2[i][j] = increment++;

			}
		}
		// Save and print images
		ImageTools.writeImageDataToFile(data1, outputfile1);
		ImageTools.writeImageDataToFile(data2, outputfile2);
		ImageTools.getImageDataFromFile("test_a1");
		ImageTools.getImageDataFromFile("test_a2");

	}

	public static void teil_b() {
		String outputfile = "test_b";
		int[][] data;

		int w = IOTools.readInteger("Breite des Bilds: ");
		int h = IOTools.readInteger("Hoehe des Bilds: ");

		// *********** hier einfuegen ***********

		data = new int[h][w];

		// fill in the left upper corner
		for (int i = 0; i < h / 2; i++) {
			for (int j = 0; j < w / 2; j++) {
				data[i][j] = 200;
			}
		}
		// fill in the right upper corner
		for (int i = 0; i < h / 2; i++) {
			for (int j = w / 2; j < w; j++) {
				data[i][j] = 0;
			}
		}
		// fill in the right lower corner
		for (int i = h / 2; i < h; i++) {
			for (int j = w / 2; j < w; j++) {
				data[i][j] = 200;
			}
		}
		// fill in the left lower corner
		for (int i = h / 2; i < h; i++) {
			for (int j = 0; j < w / 2; j++) {
				data[i][j] = 0;
			}
		}
		ImageTools.writeImageDataToFile(data, outputfile);
	}

	public static void teil_c() {

		int[][] data = ImageTools.getImageDataFromFile(SCHLOSS);
		int[][] data_neu = new int[data.length][data[0].length];
		String outputfile = "test_c";
		int schwelle = IOTools.readInteger("Schwellwert: ");

		// *********** hier einfuegen ***********

		// Make picture black-white based on "Schwelle"
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				if (data[i][j] < schwelle) {
					data_neu[i][j] = 0;
				} else {
					data_neu[i][j] = 255;
				}

			}
		}

		ImageTools.writeImageDataToFile(data_neu, outputfile);
	}

	public static void teil_d() {
		int[][] data = ImageTools.getImageDataFromFile(SCHLOSS);
		int[][] data_neu = new int[data.length][data[0].length];
		String outputfile = "test_d";

		// *********** hier einfuegen ***********

		// some useful variables
		int new_point = 0;
		int x = 0;
		int y = 0;
		int[][] temp = new int[data.length][data[0].length];

		// Make the picture grey using the given formula
		// Save it in array "temp"
		for (int i = 1; i < data.length - 1; i++) {
			for (int j = 1; j < data[0].length - 1; j++) {

				new_point = (int) Math.pow((Math.pow((data[i + 1][j] - data[i - 1][j]), 2)
						+ Math.pow((data[i][j + 1] - data[i][j - 1] - data[i][j - 1]), 2)), 0.5);
				temp[i][j] = new_point;

			}
		}
		// Extract the particular points and render them visible
		// using the given formula. Save the result as "data_neu"
		for (int i = 1; i < data.length - 1; i++) {
			for (int j = 1; j < data[0].length - 1; j++) {
				x = (temp[i + 1][j] - temp[i - 1][j]);
				y = (temp[i][j + 1] - temp[i][j - 1]);

				data_neu[i][j] = x + y;

			}
		}
		// Increase contrast between black-white
		// as in part c)
		//
		// int schwelle = IOTools.readInteger("Schwellwert: ");
		// for (int i = 0; i < data.length; i++) {
		// for (int j = 0; j < data[0].length; j++) {
		// if (data_neu[i][j] < schwelle) {
		// data_neu[i][j] = 0;
		// } else {
		// data_neu[i][j] = 255;
		// }
		//
		// }
		// }
		//
		ImageTools.writeImageDataToFile(data_neu, outputfile);
	}

	public static void main(String[] args) {
		// teil_a();
		// teil_b();
		// teil_c();
		// teil_d();
	}
}
