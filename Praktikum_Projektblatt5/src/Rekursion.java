// Friedl Frantisek, 1571906
public class Rekursion {

	// Feld mit Zufallszahlen (1 .. K) füllen
	public static void zufallsFeld(int[] feld, int K) {
		for (int i = 0; i < feld.length; i++) {
			feld[i] = (int) (Math.random() * K) + 1;
		}
	}

	public static void printFeld(int[] feld) {
		for (int i = 0; i < feld.length; i++) {
			System.out.print(feld[i] + "  ");
		}
		System.out.println();
	}

	// *** Teilaufgabe (a) ***
	public static int plusRek(int a, int b) {
		if (a == b) {
			return (a + b);
		}
		if (a < b) {
			if (a == b - 1) {
				return a + b;
			} else if (b == a + 1) {
				return a + b;
			}
			a = a + 1;
			b = b - 1;
			return plusRek(a, b);
		} else {
			if (b == a - 1) {
				return a + b;
			} else if (a == b + 1) {
				return a + b;
			}
			a = a - 1;
			b = b + 1;

			return plusRek(a, b);
		}

	}

	// *** Teilaufgabe (b) ***
	public static String print(String s, int index) {
		if (index != -1) {
			System.out.print(s.charAt(s.length() - index - 1));
			return print(s, index - 1);
		} else {
			String empty = "";
			return empty;
		}
	}

	public static String print_umgekehrt(String s, int index) {
		if (index != 0) {
			System.out.print(s.charAt(index));
			return print_umgekehrt(s, index - 1);
		} else {
			String empty = "";
			return empty;
		}
	}

	// *** Teilaufgabe (c) ***
	public static int max(int[] feld, int a, int b) {
		int index = 0;
		int result = 0; // for testing

		// first condition
		if (b - a == 0) {
			return feld[0];
		}

		// create sub-field containing values between a and b.
		int[] temp = new int[b + 1 - a];
		for (int i = 0; i <= b - a; i++) {
			temp[i] = feld[i];
		}

		// second exception
		if (temp.length == 2) {
			if (temp[index] > temp[index + 1]) {
				return temp[index];
			} else {
				return temp[index + 1];
			}

		}
		// in the third condition we want to split the array in two parts
		int[] feld1 = new int[temp.length / 2 + 1];
		int[] feld2 = new int[temp.length / 2 + 1];

		// in the third condition:

		// fill in feld1
		for (int i = 0; i < temp.length / 2; i++) {
			feld1[i] = temp[i];

		}

		// fill in feld2
		for (int i = 0; i < temp.length / 2; i++) {
			feld2[i] = temp[i + temp.length / 2];
		}

		// recursion ....
		// ...
		// Tut mir leit :(
		return result;
	}

	//
	// // *** Teilaufgabe (d) ***
	public static int rekFolge(int n) {

		if (n == 0) {
			return 1;
		} else {

			// create an array and fill it in with the appropriate numbers
			int iter = 1;
			int[] folge = new int[n];
			for (int i = 0; i < n; i++) {
				folge[i] = iter;
				iter = 2 * iter;
			}
			return folge[n - 1] + rekFolge(n - 2);
		}

	}

	// *** Teilaufgabe (e) ***
	public static long umkehrzahl_iterativ(long zahl) {
		// convert Zahl to string
		String var = Long.toString(zahl);
		String accum = "";

		// accummulate individual char. in reverse
		System.out.println("The number in reverse is: ");
		for (int i = 1; i < var.length() + 1; i++) {
			accum = accum + var.charAt(var.length() - i);
		}
		// convert result back to long
		return Long.parseLong(accum);

	}

	public static long umkehrzahl_rekursiv(long zahl) {
		if (zahl < 10) {
			return zahl;
		} else {
			System.out.print(zahl % 10);
			return umkehrzahl_rekursiv(zahl / 10);
		}

	}

	public static void main(String[] args) {
		// Test (a)
		int a = 27, b = 54;
		// plusRek(a, b);
		// System.out.println(a + " + " + b + " = " + plusRek(a, b) + "\n");

		// Test (b)
		String s = "Dies ist ein Test";
		// Die Methode length der Klasse String liefert
		// die Anzahl der Zeichen in dem String
		// print(s,s.length()-1);
		// System.out.println();
		// print_umgekehrt(s, s.length() - 1);
		// System.out.println("\n");

		// Test (c)
		int N = 5; // 50
		int[] feld = new int[N]; // Feld anlegen
		// zufallsFeld(feld, 1000); // Feld fuellen
		// printFeld(feld); // Feld ausgeben
		// int maximum = max(feld, 0, N - 1);
		// System.out.println("Das Maximum ist: " + maximum);
		// System.out.println();

		// Test (d)
		// int result = rekFolge(10);
		// System.out.println("\n");
		// System.out.println(result);
		// Test (e)
		long zahl = 123456789;
		// System.out.println(umkehrzahl_iterativ(zahl));
		// System.out.println(umkehrzahl_rekursiv(zahl));

	}
}
