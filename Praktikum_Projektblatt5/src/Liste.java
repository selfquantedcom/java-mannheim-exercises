// Friedl Frantisek, 1571906
class Termin {
	public int tag;
	public int monat;
	public int jahr;
	public String todo;

	public Termin(int t, int m, int j, String s) {
		tag = t;
		monat = m;
		jahr = j;
		todo = s;
	}

	/* *** Teilaufgabe (a) *** */
	//verify conditions starting with year 
	public boolean istVorher(Termin a) {
		if (a.jahr < this.jahr) {
			return true;
		} else if (a.jahr == this.jahr) {
			if (a.monat < this.monat) {
				return true;
			} else if (a.monat == this.monat) {
				if (a.tag < this.tag) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public String toString() {
		return tag + "." + monat + "." + jahr + "  " + todo;
	}
}

class Listenelement {
	Termin data;
	Listenelement next;
}

public class Liste {

	Listenelement head; // Pseudoelement fuer den Kopf der Liste
	Listenelement z; // Pseudoelement fuer das Ende der Liste

	// leere Liste erzeugen
	public Liste() {
		head = new Listenelement();
		z = new Listenelement();
		head.data = null;
		head.next = z;
		z.data = null;
		z.next = z;
	}

	// Ausgabe aller Elemente der Liste
	public void print() {
		Listenelement ele = head.next;
		while (ele != z) {
			System.out.println(ele.data);
			ele = ele.next;
		}
	}

	/* *** Teilaufgabe (b) *** */
	private void insertSorted(Termin termin) {
		Listenelement ele = head.next;
		Listenelement prev = head;
		boolean exists = false;
		while (ele != z) {
			exists = true;

			if (ele.next == this.z) {
				if (ele.data.istVorher(termin)) {
					// insert "e" with data in front of current element
					Listenelement e = new Listenelement();
					prev.next = e;
					e.data = termin;
					e.next = ele;

				} else {
					// insert "e" with data after the current element
					Listenelement e = new Listenelement();
					e.data = termin;
					e.next = z;
					ele.next = e;

				}

				break;
			}
			if (ele.data.istVorher(termin)) {
				// insert "e" with data in front of the element
				Listenelement e = new Listenelement();
				e.data = termin;
				e.next = ele;
				prev.next = e;
				break;
			} else {
				prev = ele;
				ele = ele.next;
			}

		}
		// To insert a first element into empty list
		if (!exists) {
			Listenelement ele1 = new Listenelement();
			head.next = ele1;
			ele1.next = z;
			ele1.data = termin;
		}
	}

	/* *** Teilaufgabe (c) *** */
	private Liste filter(int monat, int jahr) {

		Liste list = new Liste();
		Listenelement ele = head.next;
		
		while (ele != z) {
			if (ele.data.monat == monat && ele.data.jahr == jahr) {
				Listenelement e = list.head.next;

				if (e == list.z) {

					Listenelement inputElement = new Listenelement();
					inputElement.data = ele.data;
					list.head.next = inputElement;
					inputElement.next = list.z;
				} else {

					while (e != list.z) {
						if (e.next == list.z) {
							Listenelement inputElement = new Listenelement();
							inputElement.data = ele.data;
							e.next = inputElement;
							inputElement.next = list.z;
							break;
						}
						e = e.next;

					}
				}

			}
			ele = ele.next;
		}
		return list;
	}

	/* *** Teilaufgabe (d) *** */
	private static Liste mergeLists(Liste liste1, Liste liste2) {
		Liste list = new Liste();
		Listenelement ele = liste1.head.next;

		//input first list
		while (ele != liste1.z) {
			list.insertSorted(ele.data);
			ele = ele.next;
		}
		//input second list and sort
		ele = liste2.head.next;
		while (ele != liste2.z) {
			list.insertSorted(ele.data);
			ele = ele.next;
		}

		return list;
	}

	public static void main(String[] args) {
		Liste liste1 = new Liste(); // neue Liste anlegen
		// Liste füllen
		liste1.insertSorted(new Termin(14, 10, 2016, "Zahnarzt"));
		liste1.insertSorted(new Termin(15, 12, 2016, "Klausur PP1"));
		liste1.insertSorted(new Termin(19, 11, 2016, "Theater"));
		liste1.insertSorted(new Termin(1, 1, 2017, "Konzert"));
		liste1.insertSorted(new Termin(21, 12, 2016, "Vortrag"));
		System.out.println("Liste 1: ");
		liste1.print();
		System.out.println("*****************");

		Liste liste2 = new Liste(); // zweite Liste anlegen
		liste2.insertSorted(new Termin(12, 10, 2016, "Abgabetermin Bericht"));
		liste2.insertSorted(new Termin(31, 12, 2016, "EOY Party"));
		liste2.insertSorted(new Termin(1, 2, 2017, "Geburtstag Oma"));
		System.out.println("Liste 2: ");
		liste2.print();
		System.out.println("*****************");

		Liste liste3 = liste1.filter(12, 2016);
		System.out.println("Liste 3: ");
		liste3.print();
		System.out.println("*****************");
		Liste liste4 = liste2.filter(2, 2017);
		System.out.println("Liste 4: ");
		liste4.print();
		System.out.println("*****************");

		Liste liste5 = mergeLists(liste1, liste2);
		System.out.println("Liste 5: ");
		liste5.print();
		System.out.println("*****************");

	}

}
