//Frantisek Friedl, 1571906
import java.util.ArrayList;

//2a)
public class Parking {
	public static final int N = 100;
	public static boolean[] street = new boolean[N];

	public Parking() {
		for (int i = 0; i < street.length; i++) {
			street[i] = true;
		}
	}

	// 2h) Create a street with a 15 randomly positioned cars
	public Parking randomStreet() {
		Parking test = new Parking();

		for (int i = 0; i < 15; i++) {
			Car car = new Car(test.street);
			randomPark(car);
		}
		return test;
	}

	// 2b) / 2g)
	public void printStreet() {
		for (int i = 0; i < street.length; i++) {
			if (i % 5 == 0) {
				System.out.print("|");
			}
			if (street[i] == true) {
				System.out.print(". ");
			} else {
				System.out.print("X ");
			}

		}
		System.out.println();
	}

	// 2e)
	public boolean randomPark(Car car) {

		boolean condition = false;
		int count = 0;
		// when enough empty spaces are found, condition = true and continue
		for (int i = 0; i < street.length; i++) {
			// when sufficient slots have been found, terminate this loop
			if (condition == true) {
				break;
			}
			for (int j = 0; j < car.size; j++) {
				if (street[i + j] = true) {
					count++;
					// slots found, condition = true
					if (count == car.size) {
						condition = true;
						// slots not found, reset count
					} else if (j == car.size - 1) {
						count = 0;
					}
				}
			}
		}
		// when insufficient spaces, then terminate
		if (condition == false) {
			return false;
		}
		//park the car randomly 
		int i = (int) (Math.random() * 99);

		boolean status = car.park(i);

		if (!status) {
			randomPark(car);
		}
		return true;
	}

	// 2f)
	public boolean greedyPark(Car car) {

		for (int i = 0; i < street.length - car.size; i++) {
			if (street[i] == true) {
				boolean status = car.park(i);
				if (status) {
					return true;
				}

			}
		}
		return false;
	}

	// 2g)
	public boolean lotParking(Car car) {

		for (int i = 0; i < street.length - car.size; i += 5) {
			if (street[i] == true) {
				boolean status = car.park(i);
				if (status) {
					return true;
				}

			}
		}
		return false;
	}

	public static void main(String[] args) {
		Parking park = new Parking();

		// adjusting the visual representation
		System.out.println("BEFORE SIMULATION");
		for (int i = 0; i < 219; i++) {
			System.out.print("-");
		}
		System.out.println();
		System.out.println("Empty street");
		park.printStreet();
		

		// Conditions created based on 2h)!!!
		//Randomly park cars
		for (int i = 0; i < 5; i++) {
			Car car = new Car(park.street);
			park.randomPark(car);
		}
		System.out.println("5 randomly parked cars");
		park.printStreet();
		//Greedy park cars
		for (int i = 0; i < 5; i++) {
			Car car = new Car(park.street);
			park.greedyPark(car);
		}
		System.out.println("5 greedy - parked cars");
		park.printStreet();
		
		//Lot parking
		for (int i = 0; i < 5; i++) {
			Car car = new Car(park.street);
			park.lotParking(car);
		}
		System.out.println("5 cars parked by - lotParking");
		park.printStreet();

		// adjusting the visual representation
		for (int i = 0; i < 219; i++) {
			System.out.print("-");
		}
		System.out.println("");

		// The beginning of the simulation. Initialise variables
		final int RUN = 10; 
		long time_random = 0;
		long time_greedy = 0;
		long time_lot = 0;

		int random_count = 0;
		int greedy_count = 0;
		int lot_count = 0;

		// Explanation of the simulation procedure
		System.out.println("                                    SIMULATION");
		System.out.println(
				"Time requiered to park a randomly generated car by each of the methods, into a street already containing 15 randomly");
		System.out.println(
				"generated cars. Every run contains 100 000 simulations and the quoted time therefore represents the total time requiered ");
		System.out.println("to park a car 100 000 times i.e. time requiered to complete 1 Run");
		System.out.println();
		System.out.println(
				"----------------------------------PARKING METHOD-----------------------------------------------");
		System.out.println(
				"Run..............random...............greedy...................lot..................THE FASTEST");

		// The simulation
		for (int i = 0; i <= RUN; i++) {
			int iterator = 0;
			String fastest = "";

			// Identifying the fastest method
			if (time_random < time_greedy && time_random < time_lot) {
				fastest = "random";
			} else if (time_greedy < time_random && time_greedy < time_lot) {
				fastest = "greedy";
			} else if (time_lot < time_greedy && time_lot < time_random) {
				fastest = "lot";
			} else {
				fastest = "inconclusive";
			}

			// Increase count for the winner
			if (fastest.equals("random") == true) {
				random_count++;
			} else if (fastest.equals("greedy") == true) {
				greedy_count++;
			} else if (fastest.equals("lot") == true) {
				lot_count++;
			}
			// Formated printing of the results
			if (i != 0) {
				if (i > 9) {
					System.out.print(i + "................");
				} else {
					System.out.print(i + ".................");
				}
				if (time_random < 10) {
					System.out.print(time_random + "ms" + "..................");
				} else if (time_random > 99) {
					System.out.print(time_random + "ms" + "................");
				} else {
					System.out.print(time_random + "ms" + ".................");
				}
				if (time_greedy > 9) {
					System.out.print(time_greedy + "ms" + "....................");
				} else {
					System.out.print(time_greedy + "ms" + ".....................");
				}
				if (time_lot < 10) {
					System.out.print(time_lot + "ms" + "..................");
				} else {
					System.out.print(time_lot + "ms" + ".................");
				}

				System.out.print(fastest);
				System.out.println();
			}

			// reset variables for the next run
			time_random = 0;
			time_greedy = 0;
			time_lot = 0;

			// One cycle (run) of the simulation
			while (iterator < 100000) {
				// Generating a street and a car
				Parking test = park.randomStreet();
				Car car = new Car(test.street);

				// Creating identical conditions for all methods
				boolean[] random_street = test.street;
				boolean[] greedy_street = test.street;
				boolean[] lot_street = test.street;

				Car random_car = new Car(random_street);
				random_car.size = car.size;

				Car greedy_car = new Car(greedy_street);
				greedy_car.size = car.size;

				Car lot_car = new Car(lot_street);
				lot_car.size = car.size;

				long random1 = 0;
				long random2 = 0;

				long greedy1 = 0;
				long greedy2 = 0;

				long lot1 = 0;
				long lot2 = 0;

				// Measuring the time taken to park a car
				random1 = System.currentTimeMillis();
				test.randomPark(random_car);
				random2 = System.currentTimeMillis();
				time_random += (random2 - random1);

				greedy1 = System.currentTimeMillis();
				test.greedyPark(greedy_car);
				greedy2 = System.currentTimeMillis();
				time_greedy += (greedy2 - greedy1);

				lot1 = System.currentTimeMillis();
				test.lotParking(lot_car);
				lot2 = System.currentTimeMillis();
				time_lot += (lot2 - lot1);

				iterator++;

				// Determine the most efficient method when simulation is
				// completed
				if (i == RUN) {
					System.out.println(
							"-----------------------------------------------------------------------------------------------");
					System.out.print("The method being the fastest most frequently is: ");
					if (random_count > greedy_count && random_count > lot_count) {
						System.out.print("Random parking method");
					} else if (greedy_count > random_count && greedy_count > lot_count) {
						System.out.print("Greedy parking method");
					} else if (lot_count > greedy_count && lot_count > random_count) {
						System.out.print("Lot parking method");
					} else {
						System.out.print(
								"Impossible to determine based on the simulation. Please increase the final int RUN for a more robust result.");
					}
					break;
				}

			}

		} // End of Simulation

	}
}

// 2c)
class Car {
	public int size;
	public int position;
	public static ArrayList<Car> cars = new ArrayList<Car>();
	public boolean[] street;

	public Car(boolean[] street) {
		this.street = street;
		this.size = (int) (Math.random() * 3 + 3);

	}

	// 2h)
	public Car() {
		this.size = (int) (Math.random() * 3 + 3);
	}

	// 2d) Check the spaces to the right and park
	public boolean park(int position) {
		Car temp = this;
		if (position > street.length) {
			return false;
		}
		if (position + temp.size <= street.length) {
			int count = 0;
			for (int i = position; i < temp.size + position; i++) {

				if (street[i]) {
					count++;
				}

			}

			if (count == temp.size) {
				temp.position = position;
				cars.add(temp);
				for (int i = position; i < temp.size + position; i++) {
					street[i] = false;
				}

				return true;
			}

		}

		return false;

	}

	// 2h)
	public void unpark() {
		Car temp = this;

		// Empty the parking slot
		for (int i = temp.position; i < temp.position + temp.size; i++) {
			Parking.street[i] = true;
		}
		cars.remove(temp);
	}

}
