//Friedl, 1571906
package source;

public class Main {
	public static void main(String[] args) {

		Zeppelin zep = new Zeppelin(50, 100);
		zep.losfliegen();
		System.out.println();
		Fisch fi = new Fisch("Johny", 4);
		fi.bewegen();
		System.out.println();
		Kiesel kie = new Kiesel(50, 20);
		kie.uebersWasserFlitschenLassen();
	}

}
