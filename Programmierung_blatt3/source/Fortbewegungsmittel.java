//Friedl, 1571906
package source;

public class Fortbewegungsmittel {
	private double geschwindigkeit;

	public Fortbewegungsmittel() {
		super();
		this.geschwindigkeit = 0.0;
	}

	public Fortbewegungsmittel(double geschwindigkeit) {
		System.out.println("Ein Objekt wurde erzeugt " + geschwindigkeit);
		this.geschwindigkeit = geschwindigkeit;
	}

}
