//Friedl, 1571906
package source;

public class Tier {
	private String name;

	public Tier() {
		this.name = null;
	}

	public Tier(String name) {
		super();
		this.name = name;
		System.out.println("Ich heisse " + name);
	}

}
