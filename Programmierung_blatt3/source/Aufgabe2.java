package source;
//
//class Baum {
//	public int alter = 0;
//	private String scientificName = "none";
//	public static boolean evergreen = false;
//
//	public String getName() {
//		return scientificName;
//	}
//
//	private void cutLeaves() {
//		// ..
//	}
//}
//
//// 2a)
//class Kiefer extends Baum {
//	public double cones = 0;
//
//	public void test() { 
//		// -->hier einfuegen<-
//		 alter = 5; //valid - "Kiefer" is an innerclass (Unterklasse) of "Baum" and therefore has access to the public attribute "alter"
//		 Baum.scientificName = "Pinus"; //invalid - no access to the private attribute "scientificName"
//		 evergreen = false; //valid - "evergreen" is accessable as it is a "public static" variable
//		 String schlau = getName(); //valid - as it is a public method of the class "Baum"
//		 Baum.cutLeaves(); //invalid - as this is a private method
//		 scientificName = "Pinus Gruenus" //invalid as it is a private variable (also a syntax mistake - missing ";")
//
//				
//		}
//}
//
//// 2b)
//class Tanne {
//	public void anotherTest() {
//		Baum tan = new Baum();
//		// -->hier einfuegen<-
//		
//		Baum.alter = 1; //invalid - variable "alter" is not static. This wasn't problem above, because "Kiefer" EXTENDS "Baum"
//		String schlau = getName(); //invalid - method getName() is defined only of the class Baum. A reference to it is necessary
//		tan.alter = 2; //valid - as "tan" is an object of the class "Baum", it has an access to its attribute "Alter"
//		tan.cutLeaves(); //invalid - cutLeaves() is a private method
//		Kiefer.evergreen = true; //valid - as 2 lines above, this is an attribute of the class "Baum"
//		tan.cones = 3.5; //invalid - method "cones" is not defined
//
//		
//		
//		
//		
//		
//	}
//}
