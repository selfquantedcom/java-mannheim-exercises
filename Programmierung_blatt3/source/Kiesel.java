//Friedl, 1571906
package source;

public class Kiesel extends Stein {
	public double dicke;

	public Kiesel(double gewicht, double dicke) {
		super(gewicht);
		this.dicke = dicke;
		System.out.println("Ein neuer Stein. Ich bin " + dicke + " dick");
	}

	public void uebersWasserFlitschenLassen() {
		System.out.println("Ich fliege");

	}

}
