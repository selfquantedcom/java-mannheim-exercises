package source;
//Friedl Frantisek, 1571906
class Konstruktoren
{
    public static void main(String[] args)
    {
        Child adam = new Child("addi", 10);  
        adam.print(); // addi, true, 10, 1.8
        
        Child berta = new Child("berti", 20);
        berta.print(); // berti, true, 20, 1.8
        
        Child cedric = new Child("ced", 25, 1.4);
        cedric.print(); // ced, true, 25, 1.4
        
        Child doerte = new Child("doti", 1.9, 45);
        doerte.print(); // doti, false, 45, 1.9, 
        
        Child franta = new Child("franta", 1571906);
        franta.print();
    }
}

// --------------------------------------------------

class Parent {
	protected boolean finnish;
    protected int age;
    protected double height;

    Parent() {
    	// this|super?
    	finnish = true;
        age = 40;
        height = 1.8;
    }

    Parent(int age) {
    	// this|super?
    	this.age = age;
    }
    
    Parent(int age, double height) {
        // this|super?
    	this.age = age;
        this.height = height;
    }
    
    void print() {
        System.out.println("finnish:" + finnish + ", age:" + age + ", height:" + height);
    }
}

// --------------------------------------------------

class Child extends Parent {
    private String nickname;

    Child() {
        // this|super?
        nickname = "none";
    }

    Child(String nickname) {
        // this|super?
        this.nickname = nickname;
    }
    
    Child(String nickname, int age) {
    	// this|super?
    	this.age = age;
    	this.nickname = nickname;
    	
    }

    Child(String nickname, int age, double height) {
        // this|super?
        this.age = age;
        this.height = height;
    	this.nickname = nickname;
        
    }

    Child(String nickname, double height, int age) {
        // this|super?
    	this.age = age;
    	this.height = height;
    	this.nickname = nickname;
    }

    void print()
    {
    	System.out.print(nickname+ ": \t");
    	super.print();        
    }
}
