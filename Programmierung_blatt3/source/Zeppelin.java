//Friedl, 1571906
package source;

public class Zeppelin extends Fortbewegungsmittel {

	private int personenanzahl;

	public Zeppelin(double geschwindigkeit, int personenanzahl) {
		super(geschwindigkeit);
		this.personenanzahl = personenanzahl;
		System.out.println("Zeppelin wurde angerufen");

	}

	void losfliegen() {
		System.out.println("Fliegen wir los " + "mit " + this.personenanzahl + " Personen");
	}

}
