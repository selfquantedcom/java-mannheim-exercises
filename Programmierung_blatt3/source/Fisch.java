//Friedl, 1571906
package source;

public class Fisch extends Tier {
	public int anzahlBeine;

	public Fisch() {

	}

	public Fisch(String name, int anzahlBeine) {
		super(name);
		this.anzahlBeine = anzahlBeine;
		System.out.println("Ich habe " + anzahlBeine + " Beine");

	}

	public void bewegen() {
		System.out.println("Ich bewege mich");
	}

}
