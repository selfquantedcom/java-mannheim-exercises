
public class MiniFloatAdder {
	/** prints all MiniFloat values in decreasing order */
	public static void printAllMiniFloatValues() {
		// YOUR_CODE_HERE
	}

	/** MiniFloat to double */
	public static final double toDouble(byte miniFloat) {
		double result = 0;
		int exponent = 0;
		String fraction = "";
	
		// parse miniFloat into binary string.
		String number = String.format("%8s", Integer.toBinaryString(miniFloat & 0xFF)).replace(' ', '0');
		
		// Extract exponent
		String binExponent = Character.toString(number.charAt(1)) + Character.toString(number.charAt(2))
				+ Character.toString(number.charAt(3));
		exponent = Integer.parseInt(binExponent, 2);
		
		// Extract fraction
		fraction = Character.toString(number.charAt(4)) + Character.toString(number.charAt(5))
				+ Character.toString(number.charAt(6)) + Character.toString(number.charAt(7));
		
		// Convert the number when it is not normalised
		if (binExponent.equals("000")) {
			fraction = "00" + fraction;
			for (int i = 0; i < fraction.length(); i++) {
				result = result + Character.getNumericValue(fraction.charAt(i)) * 1/(Math.pow(2,i+1));
			}
			
		}

		// Convert the number when moving decimal point to the right
		if ((exponent - 3) > 0) {
			// determine digits to the left (front)/right(after) of the decimal
			// point
			String front = "1";
			String after = "";
			double decimalAfter = 0;
			double decimalFront = 0;
			for (int i = 0; i < exponent - 3; i++) {
				front = front + Character.toString(number.charAt(4 + i));
			}
			for (int i = 0; i < (number.length() - (exponent + 1)); i++) {
				after = after + Character.toString(number.charAt(exponent + 1 + i));
			}
			// convert front/after digits to decimal and add them together.
			decimalFront = Integer.parseInt(front, 2);
			for (int i = 0; i < after.length(); i++) {
				decimalAfter = decimalAfter + Character.getNumericValue(after.charAt(i)) * 1/(Math.pow(2,i+1));
			}
			result = decimalFront + decimalAfter;
	
		}
		// Convert the number when moving decimal point to the left
		else if(-3<exponent-3&&exponent-3<0) {
			if(exponent-3==-1){
				fraction = "1" + fraction;
			}else if(exponent-3==-2){
				fraction = "01" + fraction;
			}
			for (int i = 0; i < fraction.length(); i++) {
				result = result + Character.getNumericValue(fraction.charAt(i)) * 1/(Math.pow(2,i+1));
			}
		}
		//Convert the number when not moving decimal point
		else if(exponent-3==0){
			double decimalFront = 1;
			double decimalAfter = 0;
			for (int i = 0; i < fraction.length(); i++) {
				decimalAfter = decimalAfter + Character.getNumericValue(fraction.charAt(i)) * 1/(Math.pow(2,i+1));
			}
			result = decimalFront + decimalAfter;
			
		}
		// Determine sign
		if (Character.toString(number.charAt(0)).equals("1")) {
			result = result * (-1);
		}		
		
		//print result
		System.out.print(number);
		System.out.print(" "+ result);
		System.out.println();
		
		return 0;
	}

	/** adds two non-negative MiniFloats */
	public static final byte addMiniFloats(byte miniFloat1, byte miniFloat2) {
		// YOUR_CODE_HERE
		return 0;
	}

	//part b) - print number in a descending order 
	public static void main(String[] args) {
		for(int i = 127;i>=0;i--){
			toDouble((byte)i);
		}
		for(int i = -127; i<0; i++){
			toDouble((byte)i);
		}

	}

}
