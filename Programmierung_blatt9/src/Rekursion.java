//Friedl, 1571906	
public class Rekursion {

	// 1a)
	public static void print(int i) {
		if (i >= 0) {
			if (i % 4 == 0) {
				System.out.print("*");
			} else {
				System.out.print("-");
			}
			i = i - 1;
			print(i);
		}
	}

	// 1b)
	public static double arrayProd(double[] array, int start) {
		// use last position in the array to accumulate sum.
		array[array.length - 1] = array[start] + array[array.length - 1];

		start++;
		if (start < array.length - 1) {
			arrayProd(array, start);

		}
		return array[array.length - 1];

	}
	//1c)
	public static boolean isPalindrom(String word) {
		boolean result = true;
		
		// turn word to char
		char[] temp = new char[word.length()];
		for (int i = 0; i < temp.length; i++) {
			temp[i] = word.charAt(i);
		}
		// compare individual char. (int) conversion in case length is odd.

		for (int i = 0; i < (int) temp.length / 2; i++) {
			if (temp[i] == temp[temp.length - i - 1]) {
				continue;
			} else {
				result = false;
				break;
			}
		}

		return result;

	}

	public static void main(String[] args) {
		final int START = 0;
		double[] array = { 1, 2, 3, 4, 5, 6, 7 };
		String word = "tacocat"; 

		print(16);
		System.out.println();
		System.out.println(arrayProd(array, START));
		System.out.println();
		System.out.println(isPalindrom(word));
	}
}
