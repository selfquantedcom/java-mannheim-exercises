import java.util.Scanner;

public class TwosComplement {
	static boolean overflow = false;

	// add method from Blatt1 with small adjustments
	public static String add(String binary1, String binary2) {
		String result = "";
		String sum = "";
		String temp = "";
		int carry = 0;
		// match length of the numbers
		if (binary1.length() > binary2.length()) {
			temp = binary2;
			for (int i = 0; i < (binary1.length() - binary2.length()); i++) {
				temp = "0" + temp;
				if (i == (binary1.length() - binary2.length() - 1)) {
					binary2 = temp;
				}
			}
		} else if (binary1.length() < binary2.length()) {
			temp = binary1;
			for (int i = 0; i < (binary2.length() - binary1.length()); i++) {
				temp = "0" + temp;
				if (i == (binary2.length() - binary1.length() - 1)) {
					binary1 = temp;
				}
			}
		}
		// Add the numbers together by adding the last digits
		for (int i = binary1.length() - 1; i >= 0; --i) {
			int number0 = Integer.parseInt(Character.toString(binary1.charAt(i)), 2);
			int number1 = Integer.parseInt(Character.toString(binary2.charAt(i)), 2);
			sum = Integer.toBinaryString(number0 + number1);
			// Store last value of the sum depending on the carry
			if (sum.equals("10") && carry == 0) {
				result = result + "0";
				carry = 1;
			} else if (sum.equals("10") && carry == 1) {
				result = result + "1";
			} else if (sum.equals("1") && carry == 0) {
				result = result + "1";
			} else if (sum.equals("1") && carry == 1) {
				result = result + "0";
			} else if (sum.equals("0") && carry == 0) {
				result = result + "0";
			} else if (sum.equals("0") && carry == 1) {
				result = result + "1";
				carry = 0;
			}
		}
		// turn the result backward
		String revertResult = "";
		for (int i = 0; i < result.length(); i++) {
			revertResult = revertResult + Character.toString(result.charAt(result.length() - i - 1));
		}
		result = revertResult;

		return result;
	}

	// method used to calculate the last carry over value. Based on the method
	// add - will be used to calculate the overflow later
	private static int getCarry(String binary1, String binary2) {
		int result = 0;
		String sum = "";
		String temp = "";
		int carry = 0;
		// match length of the numbers
		if (binary1.length() > binary2.length()) {
			temp = binary2;
			for (int i = 0; i < (binary1.length() - binary2.length()); i++) {
				temp = "0" + temp;
				if (i == (binary1.length() - binary2.length() - 1)) {
					binary2 = temp;
				}
			}
		} else if (binary1.length() < binary2.length()) {
			temp = binary1;
			for (int i = 0; i < (binary2.length() - binary1.length()); i++) {
				temp = "0" + temp;
				if (i == (binary2.length() - binary1.length() - 1)) {
					binary1 = temp;
				}
			}
		}
		// pretend to add the numbers together and store the last carry over value
		for (int i = binary1.length() - 1; i >= 0; --i) {
			int number0 = Integer.parseInt(Character.toString(binary1.charAt(i)), 2);
			int number1 = Integer.parseInt(Character.toString(binary2.charAt(i)), 2);
			sum = Integer.toBinaryString(number0 + number1);
			// keep track of the carry
			if (sum.equals("10") && carry == 0) {
				carry = 1;
			} else if (sum.equals("0") && carry == 1) {
				carry = 0;
			}
		}
		result = carry;
		return result;
	}

	/**
	 * Addiert zwei Zahlen im 8Bit-Zweierkomplement. Die Zahlen werden im
	 * 8-Bit-Zweierkomplement uebergeben.
	 */
	public static TwosComplementResult add2(String binary1, String binary2) {
		String result = "";
		overflow = false;
		String decimal = "";

		result = add(binary1, binary2);

		// determine overflow
		int carry = getCarry(binary1, binary2);
		if ((carry + Character.getNumericValue(result.charAt(0)) + Character.getNumericValue(binary1.charAt(0))
				+ Character.getNumericValue(binary2.charAt(0))) % 2 != 0) {
			overflow = true;
		}
		

		if (result.charAt(0) == '1') {
			// turns the number into decimal
			decimal = turnIntoComplement(add(result, "11111111"));
			String help = decimal.substring(1, result.length());
			decimal = "-" + Integer.parseInt(help, 2);
		} else
			decimal = "" + Integer.parseInt(result, 2);

		return new TwosComplementResult(decimal, result, overflow);
	}

	/**
	 * Uebersetzt eine Dezimalzahl zwischen -128 und 127 in das
	 * 8-Bit-Zweierkomplement.
	 */
	public static String toBinaryString(String decimal) {
		String result = "";
		// check for a negative number
		String comparison = Character.toString(decimal.charAt(0));
		if (comparison.equals("-")) {
			String positiveNumber = "";
			// eliminate minus sign
			for (int i = 1; i < decimal.length(); i++) {
				positiveNumber = positiveNumber + decimal.charAt(i);
			}
			// convert to binary
			int convert = Integer.valueOf(positiveNumber);
			String binaryNumber = Integer.toBinaryString(convert);
			// match size to 8 bits
			if (binaryNumber.length() < 8) {
				for (int i = binaryNumber.length(); i < 8; i++) {
					binaryNumber = "0" + binaryNumber;
				}
			}
			result = turnIntoComplement(binaryNumber);
			// add 1 to turn into two complement
			result = add(result, "1");

		} else {
			// when the number is positive convert to binary only
			int convert = Integer.valueOf(decimal);
			result = Integer.toBinaryString(convert);
			// match size to 8 bits
			if (result.length() < 8) {
				for (int i = result.length(); i < 8; i++) {
					result = "0" + result;
				}
			}
		}

		return result;
	}

	private static String turnIntoComplement(String binaryNumber) {
		String result = "";
		for (int i = 0; i < binaryNumber.length(); i++) {
			if (Character.toString(binaryNumber.charAt(i)).equals("0")) {
				result = result + "1";
			} else {
				result = result + "0";
			}
		}
		return result;
	}

	// Teste deine Methoden mit zwei Dezimalzahlen, die du in der Konsole
	// eingibst
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("1. Dezimalzahl\t\t : ");
		String input1 = scan.next("-?[0-9]*");
		System.out.println("Im Zweierkomplement\t : " + toBinaryString(input1));
		System.out.print("2. Dezimalzahl:\t\t : ");
		String input2 = scan.next("-?[0-9]*");
		System.out.println("Im Zweierkomplement\t : " + toBinaryString(input2));
		scan.close();
		TwosComplementResult res = add2(toBinaryString(input1), toBinaryString(input2));
		System.out.println("Summe (ZK)\t\t : " + res.binary);
		System.out.println("Summe (Dezimal)\t\t : " + res.decimal);
		System.out.println("Overflow?\t\t : " + res.overflow);
	}

}
