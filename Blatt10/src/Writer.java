public class Writer extends Thread {

	DataObject data;
	String threadName;
	long sleepTime;

	public Writer(DataObject data, String threadName, long sleepTime) {
		this.data = data;
		this.threadName = threadName;
		this.sleepTime = sleepTime;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			long start = System.nanoTime();
			data.randomSwap();
			long end = System.nanoTime();
			System.out.println(threadName + "\t" + (end - start) / 1000000 + "ms");
			try {
				Thread.sleep(this.sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}

}
