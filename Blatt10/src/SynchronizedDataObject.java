public class SynchronizedDataObject extends DataObject {

	// Ueberschreiben Sie die Methoden von DataObject mit Hilfe des
	// Schluesselwortes synchronized so, dass ein wechselseitiger Ausschluss
	// garantiert wird. Rufen Sie dabei die ueberschriebenen Methoden mit super
	// auf.
	public SynchronizedDataObject(){
		super();
	}
	
	public synchronized int sum(){
		int sum = super.sum();
		return sum;
	}
	
	public synchronized void randomSwap(){
		super.randomSwap();
	}

}
