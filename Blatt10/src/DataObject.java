import java.util.Random;

public class DataObject {
	static final int LENGTH = 10000000;
	private int[] values = new int[LENGTH];
	Random random = new Random();

	public DataObject() {
		for (int i = 0; i < LENGTH / 2; i++) {
			values[2 * i] = i;
			values[2 * i + 1] = -i;
		} // Summe ist 0
	}

	public int sum() {
        // We compute the sum of the values after applying some function to them.
        // The function is chosen such that f(x) = -f(-x). Since every value occurs
        // twice in the data (once positive, once negative), the sum should be 0.
		int sum = 0;
		for (int i = 0; i < LENGTH; i++) {
            // we do some computation on each value to make this method CPU bound
            int v = (int)Math.round(Math.log(Math.abs(values[i])));
            sum +=  Math.signum(values[i]) * v;

            // if you want, try commenting out the two lines above and use the
            // line below instead. Then the method becomes memory-bound, i.e.,
            // you won't see much speed-up by using	multithreading
            
            //sum += values[i];
		}
        return sum;
	}

	public void randomSwap() {
		int i = random.nextInt(LENGTH);
		int j = random.nextInt(LENGTH);
		int temp = values[i];
		values[i] = values[j];
		values[j] = temp;
	}

}
