public class Reader extends Thread {

	DataObject data;
	String threadName;

	public Reader(DataObject data, String threadName) {
		this.data = data;
		this.threadName = threadName;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			long start = System.nanoTime();
			int sum = data.sum();
			long end = System.nanoTime();
			System.out.println(threadName + "\t" + (end - start) / 1000000 + "ms\tSumme=" + sum);
		}
	}

}
