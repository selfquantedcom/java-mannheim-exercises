public class Main {

	static long sleepTime() {
		// measure time for one call of sum
		DataObject dataObj = new DataObject();
		long start = System.nanoTime();
		for (int i = 0; i < 10; i++)
			dataObj.sum();
		long end = System.nanoTime();
		long sleepTime = Math.round(((end - start) / 1000000 / 10) * 0.5);
		return sleepTime;
	}

	static long run(DataObject data, long sleepTime) {
		int noReaders = 4;
		Reader[] readers = new Reader[noReaders];
		for (int i = 0; i < noReaders; i++) {
			readers[i] = new Reader(data, "Reader" + (i + 1));
		}
		Writer writer = new Writer(data, "Writer", sleepTime);

		long start = System.nanoTime();
		for (int i = 0; i < noReaders; i++) {
			readers[i].start();
		}
		writer.start();
		try {
			for (int i = 0; i < noReaders; i++) {
				readers[i].join();
			}
			writer.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long end = System.nanoTime();

		long time = end - start;
		System.out.println("Gesamtzeit: " + time / 1000000 + "ms");
		return time;
	}

	public static void main(String[] args) {
		long writerSleepTime = sleepTime();

		System.out.println("DataObject");
		System.out.println("----------");
		long time = run(new DataObject(), writerSleepTime);

		System.out.println("\nSynchronizedDataObject");
		System.out.println("---------------------");
		long timeSynchronized = run(new SynchronizedDataObject(), writerSleepTime);

		System.out.println("\nLockedDataObject");
		System.out.println("----------------");
		long timeLocked = run(new LockedDataObject(), writerSleepTime);

		System.out.println("\nZusammenfassung");
		System.out.println("---------------");
		System.out.println("DataObject            : " + time / 1000000 + "ms");
		System.out.println("SynchronizedDataObject: " + timeSynchronized / 1000000 + "ms");
		System.out.println("LockedDataObject      : " + timeLocked / 1000000 + "ms");
	}

}
