import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LockedDataObject extends DataObject {

	/** Anzahl aktueller Leser */
	int noReaders;

	/** Wahr, wenn Schreiber momentan wartend oder schreibend */
	boolean writer;

	/**
	 * Condition-Variable zum Schlafenlegen von bzw. Aufwecken des Schreibers
	 */
	Condition condWrite;

	/** Condition-Variable zum Schlafenlegen bzw. Aufwecken der Leser */
	Condition condRead;

	/**
	 * Sperre, um alle oberen Variablen zu schaetzen. Bevor auf eine der
	 * Variablen zugeriffen wird, muss ggf. diese Sperre erworben werden. Der
	 * Erwerb der Sperre soll *nur dann* erfolgen, wenn es fuer die korrekte
	 * Ausfuehrung unbedingt notwendig ist. Das ist bei einem anndernden Zugriff
	 * nur dann der Fall, wenn andere Threads die entsprechende Variable
	 * zeitgleich lesen oder schreiben koennen. Bei einem lesenden Zugriff soll
	 * die Sperre nicht erworben werden, wenn andere Threads zeitgleich nur
	 * lesend, aber nicht schreibend, auf die entsprechende Variable zugreifen
	 * koennen.
	 */
	ReentrantLock lock;

	public LockedDataObject() {
		this.lock = new ReentrantLock();
		this.condRead = lock.newCondition();
		this.condWrite = lock.newCondition();
		this.noReaders = 0;
		this.writer = false;
	}

	// NOTE: We only need to use the lock to prevent multiple writers from
	// writing at the same time. However as we may assume, that there is only 1
	// writer at a time, this situation will never occur!
	public int sum() {
		// Beachten Sie durchgaengig die korrekte Verwendung der Sperrvariable
		// "lock". Erwerben Sie die Sperre nur, falls unbedingt notwendig.

		// 1. Solange ein Schreiber wartet oder schreibt, schlafenlegen.
		if (writer == true) {
			try {
				condRead.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 2. Anzahl der Leser um 1 erhoehen.
		noReaders = noReaders + 1;

		// 3. Summe bilden
		int sum = super.sum();

		// 4. Anzahl der Leser erniedrigen und ggf. Schreiber aufwecken.
		// NOTE: assume only 1 writer -> use signal() rather than signalAll()
		noReaders = noReaders - 1;
		condWrite.signal();

		// 5. Summe zurueckgeben
		return sum;
	}

	public void randomSwap() {
		// Beachten Sie durchgaengig die korrekte Verwendung der Sperrvariable
		// "lock". Erwerben Sie die Sperre nur, falls unbedingt notwendig.

		// 1. Anzeigen, dass ein Schreiber wartet.
		writer = true;

		// 2. Schlafenlegen, solange noch mindestens ein Leser aktiv
		if (noReaders != 0) {
			try {
				condWrite.await();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 3. Elemente vertauschen
		super.randomSwap();

		// 4. Anzeigen, dass kein Schreiber mehr wartet/schreibt und
		// ggf. Leser aufwecken.
		// NOTE: Multiple readers -> use signalAll()
		writer = false;
		condRead.signalAll();
	}

}
