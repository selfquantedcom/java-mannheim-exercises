
public class Aufgabe3 {

	public static void main(String[] args) {
		int[][] my = new int[8][8];
		int count = 1; // change to "Integer" use count.toString()

		for (int i = 0; i < 8; i++) {
			System.out.println();
			count = i + 1;
			for (int j = 0; j < 8; j++) {
				my[i][j] = count++;
				System.out.print(my[i][j] + " ");

				// How do you format it? Convert to string - based on length,
				// add spaces
			}
		}

	}

}
