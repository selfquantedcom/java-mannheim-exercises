//Need to format the fields. Field2 not printed correctly!
public class Aufgabe4 {

	public static void main(String[] args) {
		int M = IOTools.readInteger("Input a positive integer: ");
		int N = IOTools.readInteger("Make the code easier and input a different positive integer ;): ");

		int K = 0;
		int[][] my = new int[M][N];

		// assign value to K
		if (M < N) {
			K = M;
		} else {
			K = N;
		}

		// Create two new arrays with dimensions K,K
		int[][] field1 = new int[K][K];
		int[][] field2 = new int[K][K];

		int test = 1;// Delete this later
		// user defines values
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print("Coordinates are: " + i + " ");
				System.out.print(" " + j + "\n");
				my[i][j] = test;// IOTools.readInteger("Input value=");
				test++;

				// input values to field1
				if (i < K & j < K) {
					field1[i][j] = my[i][j];
				}
				// input values to field2
				if (i > (M - K) & j >= (N - K) & j < K) {
					field2[i][j] = my[i][j];
				}
				

			}
		}
		// print field1
		for (int i = 0; i < field1.length; i++) {
			if (i != 0) {
				System.out.println();
			}
			for (int j = 0; j < field1[i].length; j++) {
				System.out.print(field1[i][j] + " ");
			}
		}
		// print empty space
		System.out.println();
		System.out.println();

		// print field2
		for (int i = 0; i < field2.length; i++) {
			if (i != 0) {
				System.out.println();
			}
			for (int j = 0; j < field2[i].length; j++) {
				System.out.print(field2[i][j] + " ");
			}
		}

	}

}
