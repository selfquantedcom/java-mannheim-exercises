
public class Aufgabe2 {

	public static void main(String[] args) {
		int[][] mA;
		mA = new int[4][2];
		System.out.println("The reference is: " + mA + "\n");
		System.out.println("The array looks like this: ");
		for (int i = 0; i < 4; i++) {
			if(i!=0){
				System.out.println();
			}
			for (int j = 0; j<mA[i].length; j++){
				System.out.print(mA[i][j] + " ");
			}
		}
	}

}
