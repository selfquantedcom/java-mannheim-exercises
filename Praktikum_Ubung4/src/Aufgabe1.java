
public class Aufgabe1 {

	public static void main(String[] args) {
		int[] a = { 1, 2, 3, 4 };
		int[] b = { 1, 2, 3, 4 };
		boolean same = false;

		if (a.length != b.length) {
			same = false;
		} else {
			for (int i = 0; i < a.length; i++) {
				if (a[i] == b[i]) {
					same = true;
				} else {
					same = false;
				}
			}

		}
		System.out.println(same);
	}
}