//Frantisek Friedl 1571906
//3a)
public class Enigma {

	public static int number = 0;
	public static String chiffre = "GEHEIM"; 

	public static char encryptChar(char c) {
		int myTable = (int) c - 65; // transfer from ASCII to my table value
		int result = myTable + (int) chiffre.charAt(number);
		return (char) result;
	}
//3b) note: there is a mistake in the example (U!=14 but U=20)
	public static String encryptText(String text) {
		String result = "";
		for (int i = 0; i < text.length(); i++) {
			result = result + Character.toString(encryptChar(text.charAt(i)));
			number++;
			if (number == chiffre.length()) {
				number = 0;
			}
			
		}
		return result;
	}
//3c)
	public static char decryptChar(char c){
		int myTable = (int) c + 65; 
		int result = myTable - (int) chiffre.charAt(number);
		return (char) result;
		
	}

	public static String decryptText(String text) {
		String result = "";
		for (int i = 0; i < text.length(); i++) {
			result = result + Character.toString(decryptChar(text.charAt(i)));
			number++;
			if (number == chiffre.length()) {
				number = 0;
			}
			
		}
		return result;
	}
	

	public static void main(String[] args) {
		System.out.println(decryptText("JEZBEMXBTEKTHEY"));
		System.out.println(encryptText("FRANTISEK"));
	//As most of the output is correct there doesn't seem to be a structural mistake in the code
	//Could there be a mistake in the text given for decryption?  
	}

}
