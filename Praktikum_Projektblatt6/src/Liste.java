public class Liste {
	private int amount = 3; // Anzahl der Elemente, um die data vergroessert
							// bzw. verkleinert wird
	private Object[] data;
	private int currentEndIndex = 0; // Index, an dem das naechste Objekt
										// gespeichert wird

	// initiale Liste mit n Elementen
	public Liste(int N) {
		amount = N;
		data = new Object[N];
	}

	// initiale Liste mit vorgegebener Default-Zahl an Elementen
	public Liste() {
		data = new Object[amount];
	}

	// Laenge der Liste entspricht Anzahl der Elemente, also dem Wert von
	// currentEndIndex (wo aktuell noch kein Element gespeichert wird)
	public int length() {
		return currentEndIndex;
	}

	// gibt an, ob das Feld data voll ist.
	public boolean isFull() {
		return currentEndIndex == data.length;
	}

	// gibt true zurueck, falls die Liste leer ist
	// d.h. wenn kein Objekt gespeichert ist
	// ansonsten den Wert false
	public boolean isEmpty() {
		return (currentEndIndex == 0);
	}

	// gibt den Inhalt der Liste elementweise aus
	public void printAllElements(String s) {
		System.out.println("====== " + s + " ======");
		for (int i = 0; i < currentEndIndex; i++) {
			System.out.println(data[i]);
			System.out.println("---------");
		}
		System.out.println("===================");
	}

	/* *** Aufgabenteil (a) *** */
	// Feld mit Daten um amount Elemente vergroessern bzw. verkleinern
	// n > 0 bzw. n < 0
	// und die alten Werte umkopieren
	private void resize(int n) {
		int count = 0;
		Object[] my_object = new Object[amount + n];

		for (int i = 0; i < data.length; i++) {
			if (data[i] != null) {
				my_object[count] = data[i];
				count++;
			}
		}
	}

	/* *** Aufgabenteil (b) *** */
	// am Ende einfuegen
	// falls das Feld voll ist, das Feld vorher vergroessern und
	// Inhalt des alten Felds umkopieren
	 protected void insert(Object obj){
		 
		 if(isFull()==true){
			 resize(1);
		 }
		 
		 
	 }

	/* *** Aufgabenteil (c) *** */
	// Objekt an k-ter Stelle einfügen (
	// protected void insertAt(Object obj, int k)

	/* *** Aufgabenteil (d) *** */
	// k.tes Element aus der Liste entfernen und
	// zurueckgeben bzw. null zurueckgeben, falls
	// am Index (k-1) kein Element steht
	// bzw. die Liste kleiner ist
	// falls die Liste nach dem Loeschen mehr als amount
	// freie Plaetze hat, wird das Datenfeld verkleinert
	// protected Object removeAt(int k)

	public static void main(String[] args) {
		Liste sListe = new Liste();
		// insert abc, def, ghi, ...
		// for (int i=0;i<8;i++){
		// // 1. Buchstabe 'a','d','g',...
		// String s = "" + (char)(97+i*3) + (char)(98+i*3) + (char)(99+i*3);
		// sListe.insert(s);
		// System.out.println("insert " + s);
		// }
		sListe.printAllElements("Liste 1");
		// System.out.println("removed: " + sListe.removeAt(1));
		// System.out.println("removed: " + sListe.removeAt(7));
		// System.out.println("removed: " + sListe.removeAt(2));
		// System.out.println("removed: " + sListe.removeAt(3));
		sListe.printAllElements("Liste 2");
		// sListe.removeAt(1);
		// sListe.removeAt(3);
		// sListe.removeAt(2);
		// sListe.removeAt(1);
		// sListe.removeAt(2);
		sListe.printAllElements("Liste 3");

	}

}
