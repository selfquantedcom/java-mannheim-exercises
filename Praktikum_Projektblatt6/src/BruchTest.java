public class BruchTest {

	public static void main(String args[]) {
		int z1 = IOTools.readInteger("Erster Zaehler: ");
		int n1 = IOTools.readInteger("Erster Nenner: ");
		int z2 = IOTools.readInteger("Zweiter Zaehler: ");
		int n2 = IOTools.readInteger("Zweiter Nenner: ");
		Bruch b1 = new Bruch(z1, n1);
		Bruch b2 = new Bruch(z2, n2);
		System.out.println("Kehrwert von " + b2 + " : " + b2.kehrwert());
		System.out.println("Multiplikation: " + b1.mul(b2));
		System.out.println("Division: " + b1.div(b2));
		System.out.println("Addition: " + b1.add(b2));
		System.out.println("Subtraktion: " + b1.sub(b2));
		System.out.println("Doublewert 1. Bruch: " + b1.doubleValue());
		System.out.println("gekuerzt 1. Bruch: " + b1.kuerze());
	}
}
