
public class Maler {

	public static void main(String[] args) {
		
		Square sq = new Square();
		sq.changeColor("green");
		sq.changeSize(500);
		sq.moveHorizontal(-100);
		sq.moveVertical(150);
		sq.makeVisible();
		
		Square sq2 = new Square();
		sq2.changeColor("blue");
		sq2.changeSize(400);
		sq2.moveHorizontal(-70);
		sq2.moveVertical(-225);
		sq2.makeVisible();
		
		Circle ci = new Circle(); 
		ci.changeColor("yellow"); 
		ci.changeSize(80); 
		ci.moveHorizontal(150); 
		ci.moveVertical(-20); 
		ci.makeVisible();
		
	}

}
