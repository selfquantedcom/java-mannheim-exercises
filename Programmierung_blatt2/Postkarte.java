//Friedl Frantisek, 1571906
//2a)
public class Postkarte {

	private String Inhalt;
	private double Porto;

	// 2b)
	Postkarte() {
		Inhalt = "Hallo Freunde";
		Porto = 906;
	}

	Postkarte(double Porto, String text) {
		this.Porto = Porto;
		Inhalt = text;

	}

	// 2c)
	void getParameter() {
		System.out.println(Inhalt);
		System.out.println(Porto);
	}

	// 2d)
	void frankieren(int briefMarke) {
		this.Porto = Porto + briefMarke;
	}

	// 2e)
	void copyInhalt(Postkarte A) {
		Inhalt = A.Inhalt;

	}

	public static void main(String[] args) {
		// Postkarte postA = new Postkarte(5, "Text");
		// postA.frankieren(5);
		// postA.getParameter();
		//
		// Postkarte postB = new Postkarte();
		// postB.getParameter();
		// postB.copyInhalt(postA);
		// postB.getParameter();

	}
}
