//Frantisek Friedl, 1571906
public class Matrixmultiplikation {
	// 1b)
	public static int[] matrixmanipulation(int[] vector, int[][] matrix) {
		int[] result = new int[vector.length];

		for (int i = 0; i < vector.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				result[i] = result[i] + (vector[j] * matrix[i][j]);

			}
		}

		return result;
	}

	public static void main(String[] args) {
		// 1a)
		int[] vector1 = new int[3];
		vector1[0] = 1;
		vector1[1] = -2;
		vector1[2] = 2;

		int[][] matrix1 = new int[3][3];

		matrix1[0][0] = 0;
		matrix1[0][1] = 1;
		matrix1[0][2] = 3;

		matrix1[1][0] = 3;
		matrix1[1][1] = 2;
		matrix1[1][2] = 1;

		matrix1[2][0] = 2;
		matrix1[2][1] = 1;
		matrix1[2][2] = 2;

		// 1c)

		int[] result = new int[vector1.length];
		result = matrixmanipulation(vector1, matrix1);

		System.out.print("The first matrix multiplication: ");
		System.out.print("(");
		for (int i = 0; i < vector1.length; i++) {
			System.out.print(result[i]);
			if (i != vector1.length - 1) {
				System.out.print(",");
			}
		}
		System.out.println(")");

		// 1d)
		int[] vector2 = new int[2];

		vector2[0] = 0;
		vector2[1] = -6;

		int[][] matrix2 = new int[4][2];

		matrix2[0][0] = 2;
		matrix2[0][1] = 0;

		matrix2[1][0] = 1;
		matrix2[1][1] = 1;

		matrix2[2][0] = 1;
		matrix2[2][1] = 2;

		matrix2[3][0] = 3;
		matrix2[3][1] = 1;

		int[] result2 = new int[vector2.length];
		result2 = matrixmanipulation(vector2, matrix2);
		
		System.out.print("The second matrix multiplication: ");
		System.out.print("(");
		for (int i = 0; i < vector2.length; i++) {
			System.out.print(result2[i]);
			if (i != vector2.length - 1) {
				System.out.print(",");
			}
		}
		System.out.println(")");

		// note: I have originally created it "dynamically" so that it adapts to
		// changing parameters.
		// Instead of using vector.length and matrix[0].length I could have put
		// there the appropriate numbers directly.

	}

}
