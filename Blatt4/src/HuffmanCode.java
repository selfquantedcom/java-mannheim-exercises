import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class HuffmanCode {

	/** list of all chars in the file as Character */
	private char[] chars;

	/** reads a file byte by byte and returns the file as a char[] */
	private static char[] readFile(String filename) {
		ArrayList<Character> chars = new ArrayList<Character>();
		try (FileInputStream fis = new FileInputStream(filename)) {
			int input;
			while ((input = fis.read()) != -1) {
				chars.add((char) input);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Die angegebene Datei konnte nicht gefunden werden.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		char[] result = new char[chars.size()];
		for (int i = 0; i < result.length; i++)
			result[i] = chars.get(i);
		return result;
	}

	public static void main(String[] args) {
		try {
			HuffmanCode huffman = new HuffmanCode();
			huffman.chars = readFile(args[0]);

			// OUR CODE STARTS HERE

			// How many characters are in the file?
			int numberOfChar = huffman.chars.length;

			// Store individual characters and how often they occur (frequency)
			// NOTE: ENTER IS STORED TWICE!!! THIS DISTORTS SOME OF THE ANSWERS
			ArrayList<MyCharacter> characters = new ArrayList<>();
			ArrayList<MyCharacter> characters2 = new ArrayList<>();

			for (int i = 0; i < huffman.chars.length; i++) {
				boolean exists = false;
				for (MyCharacter character : characters) {
					if (character.getCharacter().equals(String.valueOf(huffman.chars[i]))) {
						character.increaseCount();
						exists = true;
					}
				}
				if (!exists) {
					MyCharacter temp = huffman.new MyCharacter(String.valueOf(huffman.chars[i]));
					temp.increaseCount();
					characters.add(temp);
				}
			}
			// Create a duplicate ArrayList - characters2
			for (int i = 0; i < huffman.chars.length; i++) {
				boolean exists = false;
				for (MyCharacter character : characters2) {
					if (character.getCharacter().equals(String.valueOf(huffman.chars[i]))) {
						character.increaseCount();
						exists = true;
					}
				}
				if (!exists) {
					MyCharacter temp = huffman.new MyCharacter(String.valueOf(huffman.chars[i]));
					temp.increaseCount();
					characters2.add(temp);
				}
			}
			//How many different characters are in the message
			int differentCharacters = characters.size();
			
			// Erase duplicated ENTER character
			for (int i = 0; i < characters.size(); i++) {
				if (characters.get(i).ch.equals("\n")) {
					characters.remove(i);
					characters2.remove(i);
					break;
				}
			}

			// What is the optimal fixed length encoding?
			int bitsPerChar = 1;
			while (true) {
				if (Math.pow(2, bitsPerChar) < characters.size()) {
					bitsPerChar = bitsPerChar + 1;
				} else {
					break;
				}
			}
			int bitsNeeded = bitsPerChar * numberOfChar;

			// Huffman coding
			
			int currentCycle = 1;
			MyCharacter temp;

			while (characters.size() != 1) {
				// sort the ArrayList using bubble sort - ascending order
				for (int i = 0; i < characters.size() - 1; i++) {
					for (int j = 1; j < characters.size() - i; j++) {
						if (characters.get(j - 1).count > characters.get(j).count) {
							temp = characters.get(j - 1);
							characters.set(j - 1, characters.get(j));
							characters.set(j, temp);
						}
					}
				}

				// Set "stage" of the two smallest elements to the currentCycle
				// of the Huffman Tree - will only be possible for individual
				// characters

				for (int i = 0; i < characters.size(); i++) {
					if (characters.get(0).ch.equals(characters2.get(i).ch)) {
						characters2.get(i).stage = currentCycle;
					}
					if (characters.get(1).ch.equals(characters2.get(i).ch)) {
						characters2.get(i).stage = currentCycle;
					}
				}
				// increase the currentCycle when appropriate
				if (characters.get(0).ch.length() != characters.get(1).ch.length()) {
					currentCycle = currentCycle + 1;
				}

				// Merge the two smallest (by count) elements into the first
				// one. Delete the second one.
				characters.get(0).count = characters.get(0).count + characters.get(1).count;
				characters.get(0).ch = characters.get(0).ch + characters.get(1).ch;
				characters.remove(1);

				// At the end, note how many bits are needed to encode each
				// character
				if (characters.size() == 1) {
					for (int i = 0; i < characters2.size(); i++) {
						characters2.get(i).stage = currentCycle - 1 - characters2.get(i).stage;
					}
				}
			}
			// Bits needed to encode the whole message
			double totalBits = 0;
			for (int i = 0; i < characters2.size(); i++) {
				totalBits = totalBits + characters2.get(i).stage * characters2.get(i).count;
			}

			// Print
			// for (MyCharacter character : characters2) {
			// System.out.println("Character: " + character.getCharacter() + "
			// bits needed: " + character.getStage());
			// }

			// End of our code - the following has been modified to print out
			// the results
			System.out.println("Anzahl Zeichen\t\t\t : " + numberOfChar);
			System.out.println("Anzahl verschiedener Zeichen\t : " + differentCharacters);
			System.out.println(
					"Kodierung mit fester Bitl�nge\t : " + bitsNeeded + " (" + bitsPerChar + " Bits pro Zeichen)");
			System.out.println("Kodierung mit Huffman-Code\t : " + totalBits + " (" + totalBits / numberOfChar
					+ " Bits pro Zeichen)");
			System.out.println("Ersparnis (optimale feste L�nge) : " + "YOUR RESULT HERE " + "%");
			System.out.println("Ersparnis (Huffman-Code)\t : " + "YOUR RESULT HERE " + "%");
			System.out.println("Entropie\t\t\t : " + "YOUR RESULT HERE");
			System.out.println("H�ufigste Zeichen:");
			System.out.println("YOUR RESULT HERE");
		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("G�ltiger Aufruf: java HuffmanCode datei");
		}
	}

	// Used (mainly) to store a given character and its occurence (frequency)
	class MyCharacter {
		private String ch;
		private int count = 0;
		private int stage = 1;

		MyCharacter(String ch) {
			this.ch = ch;
		}

		public void increaseCount() {
			this.count = this.count + 1;
		}

		public String getCharacter() {
			return ch;
		}

		public int getIncrease() {
			return count;
		}

		public void setStage(int level) {
			this.stage = this.stage + level;
		}

		public int getStage() {
			return stage;
		}

	}

}