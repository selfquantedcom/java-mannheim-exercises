//public class Wuerfeln {
//
//	public static int wurf() {
//		return (int) (Math.random() * 6 + 1);
//	}
//	// Aufruf aus der main: Wuerfeln.wurf()
//
//	public static void main(String[] args) {
//		int wurf = 0;
//		int i = 1;
//	//Mit der while(true) methode:
//		while (true) {
//
//			wurf = Wuerfeln.wurf();
//			if (wurf == 6) {
//				System.out.println(wurf);
//				break;
//			}
//			i = i + 1;
//			System.out.print(wurf + " ");
//		}
//		System.out.println("Benotigten Wuerfel: " + i);
//	}
//}

//public class Wuerfeln {
//
//	public static int wurf() {
//		return (int) (Math.random() * 6 + 1);
//	}
//	// Aufruf aus der main: Wuerfeln.wurf()
//	// Mit der While-Schleife
//	public static void main(String[] args) {
//		int wurf = 0;
//		int i = 0;
//		
//		while(wurf!=6){
//			wurf = Wuerfeln.wurf();
//			i = i + 1;
//			System.out.print(wurf + " ");
//		}
//		System.out.println("\nBenotigten Wuerfel: " + i);
//	}
//}

public class Wuerfeln {

	public static int wurf() {
		return (int) (Math.random() * 6 + 1);
	}
	// Aufruf aus der main: Wuerfeln.wurf()
	// Mit der Do-While-Schleife
	public static void main(String[] args) {
		int wurf = 0;
		int i = 0;
		
		do {
			wurf = Wuerfeln.wurf();
			i = i + 1;
			System.out.print(wurf + " ");
		} while (wurf!=6);
		System.out.println("\nBenotigten Wuerfel: " + i);
	}
}