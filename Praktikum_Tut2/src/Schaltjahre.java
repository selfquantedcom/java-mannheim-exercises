//Teil a)
//public class Schaltjahre {
//
//	public static void main(String[] args) {
//		int input = IOTools.readInteger("Geben Sie ein Jahr ein: ");
//		while (true) {
//			if (input < 0) {
//				input = IOTools.readInteger("Geben Sie eine positive Ganzahl ein: ");
//				continue;
//			}
//			if (input % 4 == 0) {
//				if (input % 400 == 0) {
//					System.out.println("Ja es ist ein Schaltjahr!");
//					break;
//				} else if (input % 100 == 0) {
//					System.out.println("Nein, es ist kein!");
//					break;
//				} else {
//					System.out.println("Ja es ist ein Schaltjahr!");
//					break;
//				}
//			} else {
//				System.out.println("Nein, es ist kein!");
//				break;
//			}
//
//		}
//
//	}
//
//}
//Teil b)
public class Schaltjahre {

	public static void main(String[] args) {
		//Fuer Teil c) einfach aendere input Parameter
		for (int input = 1900; input<=2001; input++) {
			
			if (input % 4 == 0) {
				if (input % 400 == 0) {
					System.out.println("Ja es ist ein Schaltjahr!");
					System.out.print(input);
				} else if (input % 100 == 0) {
					System.out.println("Nein, es ist kein!");
					System.out.print(input);
				} else {
					System.out.println("Ja es ist ein Schaltjahr!");
					System.out.print(input);
				}
			} else {
				System.out.println("Nein, es ist kein!");
				System.out.print(input);
			}

		}

	}

}
