import java.util.Scanner;

public class BinaryAdder {
	// Use could have used type BigInteger to deal with large numbers
	
	public static String add(String binary1, String binary2) {
		String result = "";
		String sum = "";
		String temp = "";
		int carry = 0;
		// match length of the numbers
		if (binary1.length() > binary2.length()) {
			temp = binary2;
			for (int i = 0; i < (binary1.length() - binary2.length()); i++) {
				temp = "0" + temp;
				if (i == (binary1.length() - binary2.length() - 1)) {
					binary2 = temp;
				}
			}
		} else if (binary1.length() < binary2.length()) {
			temp = binary1;
			for (int i = 0; i < (binary2.length() - binary1.length()); i++) {
				temp = "0" + temp;
				if (i == (binary2.length() - binary1.length() - 1)) {
					binary1 = temp;
				}
			}
		}
		// Add the numbers together by adding the last digits
		for (int i = binary1.length() - 1; i >= 0; --i) {
			int number0 = Integer.parseInt(Character.toString(binary1.charAt(i)), 2);
			int number1 = Integer.parseInt(Character.toString(binary2.charAt(i)), 2);
			sum = Integer.toBinaryString(number0 + number1);
			// Store last value of the sum depending on the carry
			if (sum.equals("10") && carry == 0) {
				result = result + "0";
				carry = 1;
			} else if (sum.equals("10") && carry == 1) {
				result = result + "1";
			} else if (sum.equals("1") && carry == 0) {
				result = result + "1";
			} else if (sum.equals("1") && carry == 1) {
				result = result + "0";
			} else if (sum.equals("0") && carry == 0) {
				result = result + "0";
			} else if (sum.equals("0") && carry == 1) {
				result = result + "1";
				carry = 0;
			}
		}
		// when all digits have been added, add carry
		if (carry == 1) {
			result = result + "1";
		}
		// turn the result backward
		String revertResult = "";
		for (int i = 0; i < result.length(); i++) {
			revertResult = revertResult + Character.toString(result.charAt(result.length() - i - 1));
		}
		result = revertResult;

		// get rid of the front zeros
		temp = "";
		for (int i = 0; i < result.length() - 1; i++) {
			if (Integer.parseInt(Character.toString(result.charAt(i))) == 1) {
				for (int j = i; j < result.length(); j++) {
					temp = temp + result.charAt(j) ;
				}
				result = temp;
				break;
			} else {
				continue;
			}
		}
		return result;
	}

	// Teste deine Methode mit zwei Binaerzahlen, die du in der Konsole eingibst
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Summand:  ");
		String input1 = scan.next("(0|1)*");
		System.out.print("Summand:  ");
		String input2 = scan.next("(0|1)*");
		scan.close();
		System.out.println("Ergebnis: " + add(input1, input2));
	}

}
