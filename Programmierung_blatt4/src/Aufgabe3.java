//Friedl Frantisek, 1571906
public class Aufgabe3 {
	// a
	public static int cube(int a) {
		a = a * a * a;
		return a;
	}

	// b
	public static int divisionWithoutComma(int a, int b) {
		int result = a / b;
		return result;
	}

	// c
	public static double behindTheComma(double a, double b) {
		double result = a % b;
		return result / 2;
	}

	// d
	public static boolean isDivisble(int a, int b) {
		if (a % b == 0) {
			return true;
		} else {
			return false;
		}
	}

	// e
	public static void main(String[] args) {
		int a = cube(3);
		System.out.println(a);
		int b = divisionWithoutComma(7, 2);
		System.out.println(b);
		double c = behindTheComma(7, 2);
		System.out.println(c);
		boolean d = isDivisble(8, 4);
		System.out.println(d);
	}

}
