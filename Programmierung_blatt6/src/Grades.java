// Friedl, 1571906
public class Grades {
	public static boolean isPassed(int points) {
		boolean result = false;
		if (points >= 60) {
			result = true;
		}
		return result;
	}

	public static boolean isAwarded(int points) {
		boolean result = true;
		result = (points >= 95) ? result : !result;
		return result;
	}

	public static String gradeWithIf(int grade) { // perhaps should be "Note"
													// ... but it is irrelevant
		String result = "ungueultiger Notenwert";

		if (grade == 1) {
			result = "sehr gut";
			return result;
		} else if (grade == 2) {
			result = "gut";
			return result;
		} else if (grade == 3) {
			result = "befriedigend";
			return result;
		} else if (grade == 4) {
			result = "ausreichend";
			return result;
		} else if (grade == 5) {
			result = "mangelhaft";
			return result;
		} else if (grade == 6) {
			result = "ungenuegend";
			return result;
		}
		return result;
	}

	public static String gradeWithSwitch(int grade) {
		String result = "ungueultiger Notenwert";
		switch (grade) {
		case 1:
			result = "sehr gut";
			break;
		case 2:
			result = "gut";
			break;
		case 3:
			result = "befriedigend";
			break;
		case 4:
			result = "ausreichend";
			break;
		case 5:
			result = "mangelhaft";
			break;
		case 6:
			result = "ungenuegend";
			break;
		default:
			result = "ungueultiger Notenwert";
			break;
		}
		return result;

	}

	public static void main(String[] args) {
		System.out.println(isPassed(6));
		System.out.println(isAwarded(6));
		System.out.println(gradeWithIf(6));
		System.out.println(gradeWithSwitch(6));

	}

}
