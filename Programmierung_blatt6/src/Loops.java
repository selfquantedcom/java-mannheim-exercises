// Friedl, 1571906
public class Loops {
	public static String read(int number) {
		String result = "Only non-negative single digits are permitted";
		switch (number) {
		case 0:
			result = "null";
			break;
		case 1:
			result = "eins";
			break;
		case 2:
			result = "zwei";
			break;
		case 3:
			result = "drei";
			break;
		case 4:
			result = "vier";
			break;
		case 5:
			result = "fuenf";
			break;
		case 6:
			result = "sechs";
			break;
		case 7:
			result = "sieben";
			break;
		case 8:
			result = "acht";
			break;
		case 9:
			result = "neun";
			break;
		default:
			result = "Only non-negative single digits are permitted";
			break;
		}
		return result;

	}

	public static void readFor(int number) {
		int result = 0;
		int count = Integer.toString(number).length();
		for (int i = count; i > 0; i--) {
			result = number % 10;
			number = (int) number / 10;
			System.out.print(read(result) + ", ");

		}
	}

	public static void readWhile(int number) {
		int result = 0;
		int count = Integer.toString(number).length();
		while (count != 0) {
			result = number % 10;
			number = (int) number / 10;
			System.out.print(read(result) + ", ");
			count = count - 1;
		}
	}

	public static void readDoWhile(int number){
		int result = 0;
		int count = Integer.toString(number).length();
		do{
			 result = number % 10;
			 number = (int) number / 10;
			 System.out.print(read(result) + ", ");
			 count = count - 1;
		 }
		 while(count!=0);
	 }

	public static void main(String[] args) {
		System.out.println(read(1571906));
		readFor(1571906);
		System.out.println();
		readWhile(1571906);
		System.out.println();
		readDoWhile(1571906);
		
	}

}
