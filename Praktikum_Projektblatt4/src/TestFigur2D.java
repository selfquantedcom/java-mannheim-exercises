import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;

public class TestFigur2D extends JFrame{
    private final int size = 200;

    public TestFigur2D(Figur2D f){
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.add(new DrawingPane(f));
	this.setSize(size,size);
	this.pack();
	this.setResizable(false);
    }

    private class DrawingPane extends Canvas{
	Figur2D fig;
	public DrawingPane(Figur2D f){
	    this.setPreferredSize(new Dimension(size,size));
	    this.fig = f;
	}

	public void paint(Graphics g){
	    g.setColor(new Color(200,200,200));
	    g.fillRect(0,0,size,size);
	    g.setColor(Color.black);
	    for (Flaeche2D f : fig.flaechen){
		if (f != null){
		int n = f.anzahlEcken;
		for (int i=0;i<n-1;i++){
		    g.drawLine((int)f.ecken[i].x,(int)f.ecken[i].y,
			       (int)f.ecken[i+1].x,(int)f.ecken[i+1].y);
		}
		g.drawLine((int)f.ecken[0].x,(int)f.ecken[0].y,
			    (int)f.ecken[n-1].x,(int)f.ecken[n-1].y);
		}
	    }
	}
    }

}
