class Punkt2D {
    public double x,y;

    public Punkt2D(double a, double b){
	x = a;
	y = b;
    }

    public Punkt2D(Punkt2D p2){
	x = p2.x;
	y = p2.y;
    }

    public void verschiebe(double dx, double dy){
	x += dx;
	y += dy;
    }

    public void spiegele(double px, double py){
	if (px != 0){
	    x += 2*(px - x);
	}
	if (py != 0){
	    y += 2*(py - y);
	}
    }
}

class Flaeche2D {
    int anzahlEcken;
    Punkt2D[] ecken;

    // *** Aufgabenteil (a) ***
}

class Figur2D {
    int anzahlFlaechen;
    Flaeche2D[] flaechen;

    public Figur2D(Flaeche2D[] flaechen2){
	anzahlFlaechen = flaechen2.length;
	flaechen = new Flaeche2D[anzahlFlaechen];
	for (int i=0;i<anzahlFlaechen;i++){
	    //  flaechen[i] = new Flaeche2D(flaechen2[i]);
	}
    }

    public void show(){
	new TestFigur2D(this).setVisible(true);
    }
}

public class Aufgabe3 {

    public static void main(String[] args){
	Figur2D f;
	Flaeche2D fl1,fl2,fl3,fl4,fl5;

	double[] xKoord = {50.0, 30.0, 70.0};
	double[] yKoord = {80.0,140.0,140.0};

	// *** Aufgabenteil (b) ***

	
	// f = new Figur2D(new Flaeche2D[] {fl1, fl2, fl3, fl4, fl5});
	// f.show();
   }

}
