// Friedl, 1571906
public class Aufgabe1 {
	public static void main(String[] args) {
		Figur f = new Figur();

		// Punkte fuer Flaeche 1
		Punkt p1 = new Punkt();
		p1.x = 50;
		p1.y = 30;
		
		Punkt p2 = new Punkt();
		p2.x = 100;
		p2.y = 150;
		
		Punkt p3 = new Punkt();
		p3.x = 150;
		p3.y = 30;
	
		Punkt p4 = new Punkt();
		p4.x = 100;
		p4.y = 100;
		

		// Punkte fuer Flaeche 2
		
		Punkt p5 = new Punkt();
		p5.x = 100;
		p5.y = 150;
		
		Punkt p6 = new Punkt();
		p6.x = 175;
		p6.y = 175;
		
		Punkt p7 = new Punkt();
		p7.x=175;
		p7.y=190;
		
		Punkt p8 = new Punkt();
		p8.x = 25;
		p8.y = 190;
		
		Punkt p9 = new Punkt();
		p9.x = 25;
		p9.y = 175;
		
		// Erstellung von Flaeche 1
		Flaeche fla1 = new Flaeche();
		fla1.anzahlEcken = 4;
		fla1.ecken = new Punkt[] { p1, p2, p3, p4 };

		// Erstellung von Flaeche 2
		Flaeche fla2 = new Flaeche();
		fla2.anzahlEcken = 5;
		fla2.ecken = new Punkt[] { p5, p6, p7, p8, p9 };

		// Erstellung von der Figur
		f.anzahlFlaechen = 2;
		Flaeche[] flaechen = new Flaeche[f.anzahlFlaechen];
		flaechen[0] = fla1;
		flaechen[1] = fla2;
		f.flaechen = flaechen;

		new TestFigur(f).setVisible(true);
	}
}
